\section{Forwarding}\label{sec:fw}

NFD has a smart forwarding plane, which consists of \textbf{forwarding pipelines} (Section~\ref{sec:fw}) and \textbf{forwarding strategies} (Section~\ref{sec:strategy}).
A forwarding pipeline (or pipeline) consists of a series of processing steps on a packet. In addition, a pipeline is entered when an event is triggered and a condition is matched, such as on receiving an Interest, when detecting the received Interest was looped, when an Interest is ready to be forwarded out of a face, etc.
A forwarding strategy (or strategy) makes desicions on packet forwarding, including whether, when, and where to forward a packet.
NFD can have multiple strategies serving different namespaces, and pipelines will give packets to strategies accordingly.

Figure~\ref{fig:fw-overall} shows the overall workflow of forwarding pipelines and strategy, where blue boxes represent pipelines and white boxes represent decision points of the strategy.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.65]{fw-overall}
\caption{Pipelines and strategy: overall workflow}\label{fig:fw-overall}
\end{figure}


\subsection{Forwarding Pipelines}\label{sec:fw-pipelines}

The pipelines operate on network layer packets (Interest, Data, or Nack) and each packet is passed from one pipeline to another (in some cases through strategy decision points) until all processing is finished.
Processing within pipelines uses CS, PIT, Dead Nonce list, FIB, network region table, and Strategy Choice table, however for the last three pipelines have only read-only access, as those tables are managed by the corresponding managers and are not directly affected by data plane traffic.

\texttt{FaceTable} keeps track all active faces in NFD.
It is the entry point from which an incoming network layer packet is given to the forwarding pipelines for processing.
Pipelines are also allowed to send packets through faces.

The processing of Interest, Data, and Nack packets in NDN is quite different.
We separate forwarding pipelines into \textbf{Interest processing path}, \textbf{Data processing path}, and \textbf{Nack processing path}, described in the following sections.


\subsection{Interest Processing Path}\label{sec:fw-interest}

NFD separates Interest processing into the following pipelines:

\begin{itemize}
\item Incoming Interest: processing of incoming Interests
\item Interest loop: processing of incoming looped Interests
\item Content Store hit: processing of incoming Interests that can be satisfied by cached Data
\item Content Store miss: processing of incoming Interests that cannot be satisfied by cached Data
\item Outgoing Interest: preparation and sending out of Interests
\item Interest finalize: deleting PIT entries
\end{itemize}

\subsubsection{Incoming Interest Pipeline}\label{sec:fw-incoming-interest}

The incoming Interest pipeline is implemented in the \texttt{Forwarder::onIncomingInterest} method and is entered from the \texttt{Face::\allowbreak afterReceiveInterest} signal.
The input parameters to the incoming Interest pipeline include the newly received Interest packet and a reference to the Face on which the Interest was received.

This pipeline includes the following steps, summarized in Figure~\ref{fig:fw-incoming-interest}:

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.5]{fw-incoming-interest}
\caption{Incoming Interest pipeline}\label{fig:fw-incoming-interest}
\end{figure}

\begin{enumerate}

\item First, it is determined whether the Interest has exceeded its stored \texttt{HopLimit}~\cite{ndn-tlv}, which is similar to the time to live (TTL) field in IP packets.
      This is accomplished by checking if the \texttt{HopLimit} on the incoming Interest is zero.
      If so, the Interest is dropped.
      Otherwise, the \texttt{HopLimit} is decremented and the Interest continues on in the pipeline.

\item Next, the forwarding logic checks for \texttt{/localhost} scope~\cite{ScopeControl} violations.
      In particular, an Interest from a non-local face is not allowed to have a name that starts with \texttt{/localhost} prefix, as it is reserved for localhost communication.
      If a violation is detected, the Interest is immediately dropped and no further processing is performed.
      This check guards against malicious senders; a compliant forwarder will never send a \texttt{/localhost} Interest to a non-local face.
      Note that \texttt{/localhop} scope is not checked here, because its scope rules do not restrict incoming Interests.

\item The Name and Nonce of the incoming Interest are checked against the Dead Nonce List (Section~\ref{sec:tab-dnl}).
      If a match is found, the incoming Interest is suspected of a loop, and is given to the \textit{Interest loop pipeline} for further processing (Section~\ref{sec:fw-interest-loop}).
      If a match is not found, processing continues onto the next step.
      Note that, unlike a duplicate Nonce detected with PIT entry (described below), a duplicate detected by the Dead Nonce List does not cause the creation of a PIT entry, because creating an in-record for this incoming Interest would cause matching Data, if any, to be returned to the downstream, which is incorrect; on the other hand, creating a PIT entry without an in-record is not helpful for future duplicate Nonce detection.

\item If the Interest carries a forwarding hint, the procedure determines whether the Interest has reached the producer region, by checking if any delegation name in the forwarding hint object is a prefix of any region name from the \textit{network region table} (Section~\ref{sec:tab-nrt}).
      If so, the forwarding hint is stripped away, as it has completed its mission of bringing the Interest into the producer region, and are no longer necessary.

\item The next step is looking up existing or creating a new PIT entry, using name and selectors specified in the Interest packet.
      As of this moment, the PIT entry becomes a processing subject of the incoming Interest and following pipelines.
      Note that NFD creates the PIT entry before performing Content Store lookup.
      The main reason for this decision is to reduce the lookup overhead, on the assumption that the Content Store is likely to be significantly larger than the PIT, because in some cases described below the CS lookup can be skipped.

\item Before the incoming Interest is processed any further, its Nonce is checked against the Nonces among PIT in-records.
      If a match is found in an in-record of a different face, the incoming Interest is considered a duplicate due to either loop or multi-path arrival, and is given to \textit{Interest loop pipeline} for further processing (Section~\ref{sec:fw-interest-loop}).
      Otherwise, processing continues.
      Note that if the Nonce duplicates a Nonce previously received on the face, the Interest is considered a legit retransmission because there is no risk of persistent loop in this case.

\item Next, the \textit{expiry timer} on the PIT entry is cancelled, because a new valid Interest has arrived, so the lifetime of the PIT entry needs to be extended.
      The timer may be reset later on in the Interest processing path, for example if a matching Data is found in Content Store.

\item The pipeline then tests whether the Interest is pending, i.e., if the PIT entry already has another in-record from the same or another incoming Face.
      Recall that NFD's PIT entry can represent not only a pending Interest but also a recently satisfied Interest (Section~\ref{sec:tab-pit-entry}).
      This test is equivalent to ``having a PIT entry'' in CCN Node Model~\cite{Jacobson:2009:NNC:1658939.1658941}, whose PIT contains only pending Interests.

\item If the Interest is not pending, the Interest is matched against the Content Store (\texttt{Cs::find}, Section~\ref{sec:tab-cs-api}).
      Otherwise, CS lookup is unnecessary because a pending Interest implies that a previous CS returns no match.
      Depending on whether there's a match in CS, Interest processing continues either in \textit{Content Store miss pipeline} (Section~\ref{sec:fw-cs-miss}) or in \textit{Content Store hit pipeline} (Section~\ref{sec:fw-cs-hit}).

\end{enumerate}

\subsubsection{Interest Loop Pipeline}\label{sec:fw-interest-loop}

This pipeline is implemented in \texttt{Forwarder::onInterestLoop} method and is entered from \textit{incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}) when an Interest loop is detected.
The input parameters to this pipeline include an Interest packet, and its incoming Face.

This pipeline sends a Nack with reason code Duplicate to the Interest incoming face, if it's a point-to-point face.
Since the semantics of Nack is undefined on a multi-access link, if the incoming face is multi-access, the looping Interest is simply dropped.

\subsubsection{Content Store Hit Pipeline}\label{sec:fw-cs-hit}

This pipeline is implemented in \texttt{Forwarder::onContentStoreHit} method and is entered in the incoming Interest pipeline (Section~\ref{sec:fw-incoming-interest}), after performing a Content Store lookup (Section~\ref{sec:tab-cs-api}) and finding a match.
The input parameters to this pipeline include an Interest packet, its incoming Face, the PIT entry, and the matched Data packet.

As illustrated in Figure~\ref{fig:fw-cs-hit}, this pipeline first sets the expiry timer to now on the Interest, then invokes \textit{after Content Store hit} trigger of the chosen strategy.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.5]{fw-cs-hit}
\caption{Content Store Hit pipeline}\label{fig:fw-cs-hit}
\end{figure}

\subsubsection{Content Store Miss Pipeline}\label{sec:fw-cs-miss}

This pipeline is implemented in \texttt{Forwarder::onContentStoreMiss} method and is entered in the incoming Interest pipeline (Section~\ref{sec:fw-incoming-interest}), after performing a Content Store lookup (Section~\ref{sec:tab-cs-api}) and finding no match.
The input parameters to this pipeline include an Interest packet, its incoming Face, and the PIT entry.

As illustrated in Figure~\ref{fig:fw-cs-miss}, this pipeline takes the following steps:

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.5]{fw-cs-miss}
\caption{Content Store Miss pipeline}\label{fig:fw-cs-miss}
\end{figure}

\begin{enumerate}
\item An in-record for the Interest and its incoming face is inserted into the PIT entry.
      In case an in-record for the same incoming face already exists (i.e., the Interest is being retransmitted by the same downstream), it's simply refreshed with the newly observed Interest Nonce and expiration time.
      The expiration time of this in-record is controlled by the \texttt{InterestLifetime} field in the Interest packet; if \texttt{InterestLifetime} is omitted, the default 4~seconds is used.
\item The \textit{expiry timer} on the PIT entry is set to the time that the last PIT in-record expires.
      When the expiry timer expires, the \textit{Interest Finalize pipeline} (Section~\ref{sec:fw-interest-finalize}) is executed.
\item If the Interest carries a \texttt{NextHopFaceId} field in its NDNLPv2 header, the pipeline honors this field.
      The chosen next hop face is looked up in the FaceTable.
      If a face is found, the \textit{outgoing Interest pipeline} (Section~\ref{sec:fw-outgoing-interest}) is executed;
      if the face does not exist, the Interest is dropped.
\item Without a \texttt{NextHopFaceId} field, a forwarding strategy is responsible for making forwarding decision on the Interest.
      Therefore, the pipeline invokes Find Effective Strategy algorithm (Section~\ref{sec:tab-sc-struct}) to determine which strategy to use, and invokes the \textit{after receive Interest} trigger of the chosen strategy with the Interest packet, its incoming face, and the PIT entry (Section~\ref{sec:strategy-after-receive-interest}).
\end{enumerate}

Note that forwarding defers to the strategy the decision on whether, when, and where to forward an Interest.
Most strategies forward a new Interest immediately to one or more upstreams found through a FIB lookup.
For a retransmitted Interest, most strategies will suppress it if the previous forwarding happened recently (see Section~\ref{sec:strategy-after-receive-interest} for more details), and forward it otherwise.

\subsubsection{Outgoing Interest Pipeline}\label{sec:fw-outgoing-interest}

The outgoing Interest pipeline is implemented in \texttt{Forwarder::\allowbreak onOutgoingInterest} method and is entered from \texttt{Strategy::\allowbreak sendInterest} method which handles \textit{send Interest action} for strategy (Section~\ref{sec:strategy-send-interest}).
The input parameters to this pipeline include a PIT entry, an outgoing Face, and the Interest packet.
Note that the Interest packet is not a parameter when entering the pipeline.
The pipeline steps either use the PIT entry directly to perform checks, or obtain a reference to an Interest stored inside the PIT entry.

This pipeline includes the following steps, summarized in Figure~\ref{fig:fw-outgoing-interest}:

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.5]{fw-outgoing-interest}
\caption{Outgoing Interest pipeline}\label{fig:fw-outgoing-interest}
\end{figure}

\begin{enumerate}

\item First, it is determined whether the Interest has exceeded its \texttt{HopLimit}~\cite{ndn-tlv}.
      If an interest has a \texttt{HopLimit} of zero and the scope of the outgoing Face is non-local, it will be dropped.
      Otherwise, it will continue in the pipeline.

\item Next, an out-record is inserted into the PIT entry for the specified outgoing Face. If an out-record already exists for this same Face, it will be updated.
      Regardless of whether the record was inserted or updated, the PIT out-record remembers the Nonce of the last outgoing Interest packet (useful for matching incoming Nacks), as well as an expiration timestamp (the current time plus \texttt{InterestLifetime}).

\item Finally, the Interest is sent to the outgoing Face.

\end{enumerate}

\subsubsection{Interest Finalize Pipeline}\label{sec:fw-interest-finalize}

This pipeline is implemented in \texttt{Forwarder::onInterestFinalize} method and is entered from the \textit{expiry timer}.

The pipeline first determines whether any Nonces recorded in the PIT entry need to be inserted into the Dead Nonce List (Section~\ref{sec:tab-dnl}).
The Dead Nonce List is a global data structure designed to detect looping Interests, and we want to insert as few Nonces as possible to keep its size down.
Only outgoing Nonces (in out-records) need to be inserted, because an incoming Nonce that has never been sent out cannot loop back.

We can take further chances on the Content Store: if the PIT entry is satisfied, and the Content Store can satisfy a looping Interest (thus stop the loop) during \textit{Dead Nonce List entry lifetime} if Data packet isn't evicted, Nonces in this PIT entry don't need to be inserted.
The Content Store is believed to be able to satisfy a looping Interest, if the Interest does not have MustBeFresh, or the cached Data's FreshnessPeriod is no less than the \textit{Dead Nonce List entry lifetime}.

If it is determined that one or more Nonces should be inserted into the Dead Nonce List, tuples of Name and Nonce are added to the Dead Nonce List (Section~\ref{sec:tab-dnl-sem}).

Finally, the PIT entry is removed from the PIT.

\subsection{Data Processing Path}

Data processing in NFD is split into these pipelines:

\begin{itemize}
\item Incoming Data: processing of incoming Data packets
\item Data unsolicited: processing of incoming unsolicited Data packets
\item Outgoing Data: preparation and sending out of Data packets
\end{itemize}

\subsubsection{Incoming Data Pipeline}\label{sec:fw-incoming-data}

The incoming Data pipeline is implemented in the \texttt{Forwarder::onIncomingData} method and is entered from the \texttt{Face::\allowbreak afterReceiveData} signal.
The input parameters to this pipeline include a Data packet and the Face on which the Data packet was received.

As illustrated in Figure~\ref{fig:fw-incoming-data}, this pipeline includes the following steps:

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.5]{fw-incoming-data}
\caption{Incoming Data pipeline}\label{fig:fw-incoming-data}
\end{figure}

\begin{enumerate}

\item The first step is to check if the Data violates \texttt{/localhost} scope~\cite{ScopeControl}.
If the Data comes from a non-local Face but its name starts with \texttt{/localhost} prefix, the scope is violated and the Data is dropped.

This check guards against malicious senders; a compliant forwarder will never send a \texttt{/localhost} Data to a non-local Face.
Note that \texttt{/localhop} scope is not checked here, because its scope rules do not restrict incoming Data.

\item Then, the pipeline checks if the Data matches PIT entries, by using Data Match algorithm (Section~\ref{sec:tab-pit-table}).
If no matching PIT entry is found, the Data is given to \textit{Data unsolicited pipeline} (Section~\ref{sec:fw-data-unsolicited}); if matching PIT entries are found, the Data is inserted into the Content Store.
Note that even if the pipeline inserts the Data into the Content Store, whether the Data is actually stored and how long it stays in the Content Store is determined by the Content Store admission and replacement policies.%
\footnote{The current implementation has a fixed ``admit all'' admission policy and uses LRU as the default replacement policy, see Section~\ref{sec:tab-cs}.}

\item Next, the pipeline checks if only one matching PIT entry is found or more than one matching PIT entry is found. This check decides if forwarding strategy is capable of manipulating Data forwarding. Normally, only one matching PIT entry would be found. More than one matching PIT entry means more than one forwarding strategy is possible to manipulate Data forwarding, which is forbidden to avoid potential conflicts among strategies.

\item If only one matching PIT entry is found, meaning that only one forwarding strategy is controlling Data forwarding, the pipeline will set the PIT expiry timer to now, invoke \textit{after receive Data} trigger of the strategy, mark PIT satisfied, insert dead nonce list if needed, and clear PIT entry's out records.

\item If more than one matching PIT entry is found, for each matching PIT entry, the pipeline will remember its pending downstreams, set the PIT expiry timer to now, invoke \textit{before satisfy Interest} trigger of the strategy, mark PIT satisfied, insert dead nonce list if needed, and clear PIT entry's in and out records. Finally, the pipeline will forward the Data to each pending downstream, except that if the pending downstream Face is the same as the incoming Face of the Data, and the Face is not ad-hoc.

\end{enumerate}

\subsubsection{Data Unsolicited Pipeline}\label{sec:fw-data-unsolicited}

This pipeline is implemented in \texttt{Forwarder::onDataUnsolicited} method and is entered from the \textit{incoming Data pipeline} (Section~\ref{sec:fw-incoming-data}) when a Data packet is found to be unsolicited.
The input parameters to this pipeline include a Data packet, and its incoming Face.

The current \textit{unsolicited Data policy} is consulted to decide whether to drop the Data, or add it to the Content Store.
By default, NFD forwarding is configured with a ``drop-all'' policy which drops all unsolicited Data, as they pose a security risk to the forwarder.

There can be cases where unsolicited Data packets need to be accepted.
The policy can be changed in NFD configuration file at \texttt{tables.cs\_unsolicited\_policy} key.

\subsubsection{Outgoing Data Pipeline}\label{sec:fw-outgoing-data}

This pipeline is implemented in \texttt{Forwarder::onOutgoingData} method and pipeline is entered from \textit{incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}) when a matching Data is found in Content Store and from \textit{incoming Data pipeline} (Section~\ref{sec:fw-incoming-data}) when the incoming Data matches more than one PIT entry.
The input parameters to this pipeline include a Data packet, and the outgoing Face.

This pipeline contains the following steps:

\begin{enumerate}
\item The Data is first checked for \texttt{/localhost} scope~\cite{ScopeControl}:
Data packets with a \texttt{/localhost} prefix cannot be sent to a non-local face.%
\footnote{This check is only useful in a specific scenario (see \url{https://redmine.named-data.net/issues/1644}).}
The \texttt{/localhop} scope is not checked here, because its scope rules do not restrict outgoing Data.
\item The next step is reserved for the traffic manager actions, such as to perform traffic shaping, etc.
The current version does not include any traffic management, but it is planned to be implemented in a future release.
\item Finally, the Data packet is sent via the outgoing Face.
\end{enumerate}


\subsection{Nack Processing Path}\label{sec:fw-nack}

Nack processing in NFD is split into these pipelines:

\begin{itemize}
\item Incoming Nack: processing of incoming Nacks
\item Outgoing Nack: preparation and sending out of Nacks
\end{itemize}

\subsubsection{Incoming Nack Pipeline}\label{sec:fw-incoming-nack}

The incoming Nack pipeline is implemented in the \texttt{Forwarder::onIncomingNack} method and is entered from the \texttt{Face::\allowbreak afterReceiveNack} signal.
The input parameters to this pipeline include a Nack packet and the Face on which the Nack was received.

First, if the incoming face is not a point-to-point face, the Nack is dropped without further processing, because the semantics of Nack is only defined on a point-to-point link.

The Interest carried in the Nack and its incoming face is used to locate a PIT out-record for the same face where the Interest has been forwarded to, and the last outgoing Nonce was same as the Nonce carried in the Nack.
If such an out-record is found, it's marked \textit{Nacked} with the Nack reason.
Otherwise, the Nack is dropped because it's no longer relevant.

The effective strategy responsible for the PIT entry is determined using Find Effective Strategy algorithm (Section~\ref{sec:tab-sc-struct}).
The selected strategy is then triggered for the \textit{after receive Nack} procedure with the Nack packet, its incoming Face, and the PIT entry (Section~\ref{sec:strategy-after-receive-nack}).

\subsubsection{Outgoing Nack Pipeline}\label{sec:fw-outgoing-nack}

The outgoing Nack pipeline is implemented in \texttt{Forwarder::onOutgoingNack} method and is entered from \texttt{Strategy::\allowbreak sendNack} method which handles \textit{send Nack action} for strategy (Section~\ref{sec:strategy-send-nack}).
The input parameters to this pipeline include a PIT entry, an outgoing Face, and the Nack header.

First, the PIT entry is queried for an in-record of the specified outgoing face (downstream).
This in-record is necessary because protocol requires the last Interest received from the downstream, including its Nonce, to be carried in the Nack packet.
If no in-record is found, abort this procedure, because the Nack cannot be sent without this Interest.

Second, if the downstream is not a point-to-point face, abort this procedure, because the semantics of Nack is only defined on a point-to-point link.

After both checks are passing, a Nack packet is constructed with the provided Nack header and the Interest from the in-record, and sent through the face.
The in-record is erased as it has been ``satisfied'' by the Nack, and no further Nack or Data should be sent to the same downstream unless there's a retransmission.


\subsection{Helper Algorithms}\label{sec:fw-helper}

Several algorithms used in forwarding pipelines and multiple strategies are implemented as helper functions.
As we identify more reusable algorithms, they will be implemented as helper functions as well, rather than repeating the code in several places.

\texttt{nfd::fw::wouldViolateScope} determines whether forwarding an Interest out of a face would violate namespace-based scope control.

\texttt{nfd::fw::findDuplicateNonce} searches a PIT entry to see if there's a duplicate Nonce in any in-record or out-record.

\texttt{nfd::fw::hasPendingOutRecords} determines whether a PIT entry has an out-record that is still pending, i.e. neither Data nor Nack has come back.

\subsubsection{FIB lookup}\label{sec:fw-fib-lookup}

\texttt{Strategy::lookupFib} implements a FIB lookup procedure with consideration of forwarding hint.
The procedure is:

\begin{enumerate}
\item If the Interest does not carry a forwarding hint and hence does not require mobility processing, FIB is looked up using Interest Name (Section~\ref{sec:tab-fib-struct}, Longest Prefix Match algorithm).
      FIB guarantees that Longest Prefix Match returns a valid FIB entry; however, a FIB entry may contain empty set of NextHop records, which could effectively result (but, strictly speaking, is not required to happen) in the strategy rejecting the Interest.
\item If the Interest carries a forwarding hint\footnote{Presence of a forwarding hint at this point indicates the Interest has not reached the producer region, because the forwarding hint should have been stripped in \textit{incoming Interest pipeline} when entering the producer region.}, it is processed for mobility support.
\item The procedure looks up the FIB using each delegation name contained in the forwarding hint, and returns the first matched FIB entry that has at least one nexthop.
      It does not distinguish whether the Interest is in consumer region or default-free zone.
\item In case none of the delegation names match a FIB entry with at least one nexthop, an empty FIB entry is returned.
\end{enumerate}

A limitation of current implementation is that, when an Interest reaches the first default-free router, which delegation to use is solely determined by this FIB lookup procedure according to the routing cost in the FIB.
Ideally, this choice should be made by the strategy which can take current performance of different upstreams into consideration.
We are exploring a better design in this aspect.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
