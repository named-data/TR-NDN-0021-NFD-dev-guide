\subsubsection{ASF Strategy}\label{sec:strategy-asf}

Most strategies in NFD focus on the ability to retrieve content, but may not always find the lowest delay path.
To facilitate optimal forwarding in hyperbolic routing~(HR) and address drawbacks~(embedding issues, network dynamics, etc.), Adaptive Smoothed RTT-based Forwarding~(ASF)~\cite{AsfStrategy} is able to make forwarding decisions based on Data retrieval delay and must be able to learn the Data retrieval delay of alternative paths.

The strategy can measure the time between when an Interest is forwarded and Data is received, the round-trip time~(RTT);
to accommodate for fluctuations in RTT values, the strategy can compute and maintain a Smoothed RTT~(SRTT), a moving average of the RTT samples~(SRTT calculation is described below).
An SRTT measurement must be maintained for each next hop under a namespace.
ASF chooses the best next hop based on SRTT measurement, and also periodically probes other next hops to learn their RTTs.

This strategy is implemented as \texttt{nfd::fw::AsfStrategy} class.

\paragraph{Strategy Design}

The design of the adaptive forwarding strategy is composed of two components:

\subparagraph{SRTT-based Forwarding}
When a Data packet is received, the RTT is recorded (time between Interest forwarded and Data received).
This RTT is used to compute an SRTT; an SRTT measurement is calculated and associated with the FIB entry which the Interest matched.

When an interest arrives, all available next hops from the FibEntry are divided into three distinct groups.

\begin{itemize}
\item Group 1: Next hops with SRTT values
\item Group 2: Next hops that do not have measurements
\item Group 3: Next hops that timed-out on the previous Interest
\end{itemize}
The strategy will then try to pick a next hop using the SRTTs or assigned routing protocol costs in the following way:

\begin{itemize}
\item If Group 1 is not empty, pick the next hop with the lowest SRTT
\item Otherwise, if Group 2 is not empty, pick the next hop with the lowest routing cost
\item Otherwise, pick next hop with the lowest routing cost from Group 3
\end{itemize}

\subparagraph{SRTT-based Probing}
The first probe for a namespace is scheduled after the first Interest which matches the FIB entry arrives.
The first probe is scheduled at a random interval from [0, 5] seconds.
After the initial probe, probing is scheduled to occur periodically every 60 seconds.
If the measurements for a FIB entry expire, the probing module will consider the next Interest which creates measurements for the FIB entry as the first Interest received (i.e., the probe will be scheduled at a random interval from [0, 5] seconds).

If probing is due but no Interests arrive under that namespace, probing will not occur until a matching Interest arrives.
Probes use the same name as the triggering Interest but use a different nonce.
A different nonce is used to ensure that if the probed path is able to reach the Data, the Data will be returned on the probed path as well as the primary path.

The steps for probing are defined as follows using the same next hop grouping described in the previous section:

\begin{itemize}
\item Pick the next hop with the lowest routing cost from Group 2
\item Otherwise, sort the next hops in Group 1 by SRTT and Group 3 by routing cost; the priority of Group 1 is higher than Group 3
\item Assign a probing probability to each next hop and probabilistically select a next hop for probing
\end{itemize}

\noindent
The probing probability of the next hop with rank $i = 1, 2, \dots, N$ in the sorted list is assigned:
\begin{equation}
P(i) = 2 \cdot \frac{N + 1 - i}{N \cdot (N + 1)}
\end{equation}
An alternative formula to assign probability that could be implemented in the future is:
\begin{equation}
P(i) = \frac{2^{N - i}}{2^N - 1}
\end{equation}
An example of probing probability calculation is shown in Figure~\ref{fig:fw-asf-probing-probability}.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{figures/fw-asf-probing-probability}
\caption{Probability calculation and assignment for list of sorted NextHops.}\label{fig:fw-asf-probing-probability}
\end{figure}

\paragraph{Detailed Behavior}

\subparagraph{FaceInfo Measurements}
The FaceInfo data structure contains information used by the strategy for each Face in a namespace.
FaceInfo maintains the event ID for its scheduled RTO of the last Interest forwarded, the name of the last Interest forwarded, and the Face's RTT stats (i.e., last RTT and SRTT).

\subparagraph{NamespaceInfo Measurements}
The NamespaceInfo data structure contains a map which maps a Face's ID to its FaceInfo measurements, a boolean indicating whether or not the namespace has been probed yet, and a boolean indicating if probing is due for the namespace.

\subparagraph{Lifetime and Expiration}
Both the FaceInfo Measurements and the NamespaceInfo Measurements have a lifetime of 300 seconds. NamespaceInfo Measurements should be created or refreshed when an Interest matches a FIB entry,
when an Interest is forwarded for a namespace, and when an Interest times out for a namespace. FaceInfo Measurements should be created or refreshed when an Interest is forwarded to a Face and
when Data is received on a Face.

\subparagraph{After Receive Interest Trigger}
When receiving an Interest, ASF should first check if the received Interest should be suppressed.
The ASF strategy determines if an Interest should be suppressed using the same logic as in the BestRoute Strategy v4.
ASF should then determine if the Interest should be rejected due to the FibEntry having no next hops.
The strategy next will pick a Face to use for forwarding based on the logic outlined in the forwarding section above.

The Interest will be forwarded to the selected Face, and the measurements and RTO are updated.
The associated FaceInfo measurement's lifetime is extended for 300 seconds, and if an RTO is not already scheduled for the Face, an RTO is scheduled using the RttEstimator.

Finally, the strategy should check if the namespace is due for probing.
If so, the strategy picks a best Face to probe and forwards the Interest with a new nonce to the selected Face.
The same steps to updated the FaceInfo measurement's lifetime and RTO are followed for the probe Interest.
The namespace is then no longer due for probing, and the next probe is scheduled in 60 seconds.

\subparagraph{Before Satisfy Interest Trigger}
The strategy should use the incoming Data's name to perform a longest prefix match to retrieve the NamespaceInfo measurements.
If the NamespaceInfo measurements do not exist, the inability to retrieve the measurements is logged and the method returns.
Otherwise, the FaceInfo is retrieved from the NamespaceInfo using the incoming Face's ID, the RTT is recorded, and the FaceInfo measurement's lifetime is extended.
If the last Interest forwarded to the Face is a prefix of the incoming Data's name, the RTO is canceled.

\subparagraph{RTO Interest Timeout}
If an Interest with an RTO scheduled on a Face times out, the FaceInfo measurements are retrieved; the RTT is set to \texttt{TIMEOUT} and the RTO information is cleared.
In some cases, a number of ``silent'' timeouts may be configured for the strategy, which means the strategy waits for the specified number of successive timeouts before setting the RTT to \texttt{TIMEOUT}.

\subparagraph{Nack Support}
On an incoming Interest, if the relevant FIB entry does not have any next hops, the strategy will send a NoRoute Nack.
All received Nacks are handled by setting the timeout flag in the corresponding face's measurements.

\subparagraph{Consumer Retransmissions}
The ASF strategy follows the same suppression logic as implemented in the BestRoute Strategy v4.
On retransmission outside the suppression period, the strategy processes the Interest using the same logic as described in the forwarding section above.

\subparagraph{Router Retransmissions}
The ASF strategy does not perform router-based retransmissions.

\subparagraph{SRTT Calculation}
When an RTT measurement is added to a FaceInfo Measurement's RttEstimator, the SRTT is also updated.
If the RTT measurement is the first sample for this FaceInfo Measurement, the SRTT is set equal to the RTT.
Otherwise, the SRTT is computed as follows:
\begin{equation}
\alpha \times RTT sample + (1 - \alpha) \times previous SRTT;\ \alpha = 0.125
\end{equation}
