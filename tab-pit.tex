\subsection{Interest Table (PIT)}\label{sec:tab-pit}

The Interest Table (PIT) keeps track of Interests forwarded upstream toward content source(s), so that Data can be sent downstream to its requester(s)~\cite{Jacobson:2009:NNC:1658939.1658941}.
It also contains recently satisfied Interests for loop detection and measurements purposes.
This data structure is called ``pending Interest table'' in NDN literatures; however, NFD's PIT contains both pending Interests and recently satisfied Interests, so ``Interest table'' is a more accurate term, but the abbreviation ``PIT'' is kept.

PIT is a collection of PIT entries, used only by forwarding (Section~\ref{sec:fw}).
The structure and semantics of PIT entry, and how it's used by forwarding are described in Section~\ref{sec:tab-pit-entry}.
The structure and algorithms of PIT, and how it's used by forwarding are described in Section~\ref{sec:tab-pit-table}.
The implementation of PIT algorithms is discussed in Section~\ref{sec:tab-nt}.


\subsubsection{PIT Entry}\label{sec:tab-pit-entry}

Figure~\ref{fig:pit-struct} shows the PIT, PIT entries, in-records, out-records, and their relations.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.7]{pit-struct}
\caption{PIT and related entities}\label{fig:pit-struct}
\end{figure}

\paragraph{PIT entry}\

A PIT entry (\texttt{nfd::pit::Entry}) represents either a pending Interest or a recently satisfied Interest.
Two Interest packets are \textit{similar} if they have same Name and same Selectors~\cite{ndn-tlv}.
Multiple similar Interests share the same PIT entry.

Each PIT entry is identified by an Interest.
All fields in this Interest, except Name and Selectors, are insignificant.

Each PIT entry contains a collection of in-records, a collection of out-records, and a timer, described below.
In addition, forwarding strategy is allowed to store arbitrary information on PIT entry, in-records, and out-records (Section~\ref{sec:strategy-storage}).

\paragraph{In record}\

An \textbf{in-record} (\texttt{nfd::pit::InRecord}) represents a downstream face for the Interest.
A downstream face is a requester for the content: Interest comes from downstream, and Data goes to downstream.

The in-record stores:
\begin{itemize}
\item a reference to the face
\item the Nonce in the last Interest packet from this face
\item the timestamp on which the last Interest packet from this face arrives
\item the last Interest packet
\end{itemize}

An in-record is inserted or updated by \textit{incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}).
All in-records are deleted by \textit{incoming Data pipeline} (Section~\ref{sec:fw-incoming-data}) when a pending Interest is satisfied.

An in-record \textit{expires} when \texttt{InterestLifetime} has elapsed after the last Interest packet arrives.
A PIT entry expires when all in-records expire.
A PIT entry is said to be \textit{pending} if it contains at least one unexpired in-record.

\paragraph{Out record}\

An \textbf{out-record} (\texttt{nfd::pit::OutRecord}) represents an upstream face for the Interest.
An upstream face is a potential content source: Interest is forwarded to upstream, and Data comes from upstream.

The out-record stores:
\begin{itemize}
\item a reference to the face
\item the Nonce in the last Interest packet to this face
\item the timestamp on which the last Interest packet to this face is sent
\item Nacked field: indicates the last outgoing Interest has been Nacked; this field also records the Nack reason
\end{itemize}

An out-record is inserted or updated by \textit{outgoing Interest pipeline} (Section~\ref{sec:fw-outgoing-interest}).
An out-record is deleted by \textit{incoming Data pipeline} (Section~\ref{sec:fw-incoming-data}) when a pending Interest is satisfied by a Data from that face.

An out-record \textit{expires} when \texttt{InterestLifetime} has elapsed after the last Interest packet is sent.

\paragraph{Timer}\

Each PIT entry has one timer, the \textit{expiry timer}. This timer is used by the forwarding pipelines (Section~\ref{sec:fw}) and it fires when the PIT entry expires (Section~\ref{sec:fw-incoming-interest}).


\subsubsection{PIT}\label{sec:tab-pit-table}

The PIT (\texttt{nfd::Pit}) is a table containing PIT entries, indexed by \textless Name,Selectors\textgreater tuple.
The usual insert and delete operations are supported.
\texttt{Pit::insert} method first looks for a PIT entry for similar Interest, and inserts one only if it does not already exist; there is no separate method for exact match, because forwarding does not need to determine the existence of a PIT entry without inserting it.
The PIT is not iterable, because this is not needed by forwarding.

\textbf{Data Match} algorithm (\texttt{Pit::findAllDataMatches}) finds all Interests that a Data packet can satisfy.
It takes a Data packet as input parameter.
The return value is a collection of PIT entries that can be satisfied by this Data packet.
This algorithm does not delete any PIT entry.

\texttt{cleanupOnFaceRemoval} function is a joint FIB-PIT cleanup function upon face removal.
See Section~\ref{sec:tab-fib-struct} for more information.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
