\subsection{NameTree}\label{sec:tab-nt}

The NameTree is a common index structure for FIB (Section~\ref{sec:tab-fib}), PIT (Section~\ref{sec:tab-pit}, Strategy Choice table (Section~\ref{sec:tab-sc}, and Measurements table (Section~\ref{sec:tab-m}).
It is feasible to use a common index, because there are much commonality in the index of these four tables: FIB, Strategy Choice, and Measurements are all indexed by Name, while PIT is indexed by Name and Selectors~\cite{ndn-tlv}.
It is beneficial to use a common index, because lookups on these four tables are often related (eg. FIB longest prefix match is invoked in \textit{incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}) after inserting a PIT entry), and using a common index can reduce the number of index lookups during packet processing; the amount of memory used by the index(es) is also reduced.

NameTree data structure is introduced in Section~\ref{sec:tab-nt-struct}.
NameTree operations and algorithms are described in Section~\ref{sec:tab-nt-alg}.
Section~\ref{sec:tab-nt-shortcut} describes how NameTree can help reducing number of index lookups by adding shortcuts between tables.


\subsubsection{Structure}\label{sec:tab-nt-struct}

The NameTree is a collection of NameTree entries, indexed by Name and organized in a tree structure.
FIB, PIT, Strategy Choice, and Measurements entries are attached onto NameTree entry.

\paragraph{NameTree entry}\

A NameTree entry (\texttt{nfd::name\_tree::Entry}) contains:

\begin{itemize}[nosep]
\item the name prefix
\item a pointer to the parent entry
\item pointers to child entries
\item zero or one FIB entry
\item zero or more PIT entries
\item zero or one Strategy Choice entry
\item zero or one Measurements entry
\end{itemize}

NameTree entries form a tree structure via parent and children pointers.
The tree structure follows name hierarchy: the name prefix of a parent entry is the name prefix of its child minus the last component.

The FIB, Strategy Choice, and Measurements entries attached to a NameTree entry have the same name as the NameTree entry.
In most cases, PIT entries attached to a NameTree entry can have the same name as the NameTree entry and differ only in Selectors;
as a special case, a PIT entry whose Interest name ends with an implicit digest component is attached to the NameTree entry that corresponds to the Interest name minus the implicit digest component, so that the \textit{all match} algorithm (Section~\ref{sec:tab-nt-alg}) with an incoming Data name (without computing its implicit digest) can find this PIT entry.

\paragraph{NameTree hash table}\

NameTree entries are organized into a hash table, in addition to the tree structure, to enable faster name-based lookups.
Specifically, the hash table allows us to locate a deep entry without going from the root of the tree.
We decide to implement the hash table from scratch (\texttt{nfd::name\_tree::Hashtable}), rather than using an existing library, so that we can have better control for performance tuning.

The hash table contains a number of \textit{buckets}.
To insert an entry, we use CityHash~\cite{cityhash} to compute the hash value of its name prefix; this hash function is chosen due to its fast speed.
Specifically, hash value is computed over the TLV representation of the name components, but does not cover outer \texttt{NAME-TYPE TLV-LENGTH} fields; this allows us to compute hash values of all prefixes of a name together when needed.
The entry is then mapped into a bucket chosen by the hash value.
In case multiple names are mapped to the same bucket, we resolve hash collision by chaining the entries in a doubly linked list.

The hash table is resized automatically as the number of stored NameTree entries changes.
Resize operations are controlled by parameters in \texttt{nfd::name\_tree::HashtableOptions}.
The parameter setting is a trade-off between wasted memory of empty buckets and time overhead of chaining.
When the load factor (number of entries divided by number of buckets) goes above the expand threshold or below the shrink threshold, a resize operation is triggerred, in which every NameTree entry is moved to the appropriate bucket in the new hash table.

We introduce a \texttt{nfd::name\_tree::Node} to store fields used in hash table implementation, including:

\begin{itemize}[nosep]
\item the hash value, so that resize operation does not need to re-compute it
\item pointer to previous node in doubly linked list of the bucket
\item pointer to next node in doubly linked list of the bucket
\item the NameTree entry (Node owns Entry; Entry has a pointer back to the Node)
\end{itemize}


\subsubsection{Operations and Algorithms}\label{sec:tab-nt-alg}

\paragraph{Insertion and Deletion operations}\

The \textbf{lookup/insertion} operation (\texttt{NameTree::lookup}) finds or inserts an entry for a given Name.
To maintain the tree structure, ancestor entries are inserted if necessary.
This operation is called when a FIB/\allowbreak PIT/\allowbreak Strategy~Choice/\allowbreak Measurements entry is being inserted.

The \textbf{conditional deletion} operation (\texttt{NameTree::eraseEntryIfEmpty}) deletes an entry if no FIB/\allowbreak PIT/\allowbreak Strategy~Choice/\allowbreak Measurements entry is stored on it, and it has no children; ancestors of the deleted entry are also deleted if they meet the same requirements.
This operation is called when a FIB/\allowbreak PIT/\allowbreak Strategy~Choice/\allowbreak Measurements entry is being deleted.

\paragraph{Matching algorithms}\

The \textbf{exact match} algorithm (\texttt{NameTree::findExactMatch}) finds the entry with a specified Name, or returns null if such entry does not exist.

The \textbf{longest prefix match} algoritm (\texttt{NameTree::findLongestPrefixMatch}) finds the entry of longest prefix match of a specified Name, filtered by an optional \textit{EntrySelector}.
An EntrySelector is a predicate that decides whether an entry can be accepted (returned).
This algorithm is implemented as: start from looking up the full Name in the hash table; if no NameTree entry exists or it's rejected by the predicate, remove the last Name component and lookup again, until an acceptable NameTree entry is found.
This algorithm is called by FIB longest prefix match algorithm (Section~\ref{sec:tab-fib-struct}), with a predicate that accepts a NameTree entry only if it contains a FIB entry.
This algorithm is called by Strategy Choice find effective strategy algorithm (Section~\ref{sec:tab-sc-struct}), with a predicate that accepts a NameTree entry only if it contains a Strategy Choice entry.

The \textbf{all match} algorithm (\texttt{NameTree::findAllMatches}) enumerates all entries that are prefixes of a given Name, filtered by an optional \textit{EntrySelector}.
This algorithm is implemented as: perform a longest prefix match first; remove the last Name component, until reaching the root entry.
This algorithm is called by PIT data match algorithm (Section~\ref{sec:tab-pit-table}).

\paragraph{Enumeration algorithms}\

The \textbf{full enumeration} algorithm (\texttt{NameTree::fullEnumerate}) enumerates all entries, filtered by an optional \textit{EntrySelector}.
This algorithm is used by FIB enumeration and Strategy Choice enumeration.

The \textbf{partial enumeration} algorithm (\texttt{NameTree::partialEnumerate}) enumerates all entries under a specified Name prefix, filtered by an optional \textit{EntrySubTreeSelector}.
An EntrySelector is a double-predicate that decides whether an entry can be accepted, and whether its children shall be visited.
This algorithm is used during runtime strategy change (Section~\ref{sec:strategy-storage}) to clear StrategyInfo items under a namespace changing ownership.


\subsubsection{Shortcuts}\label{sec:tab-nt-shortcut}

One benefit of the NameTree is that it can reduce the number of index lookups during packet forwarding.
To achieve this goal, one method is to let forwarding pipelines perform a NameTree lookup explicitly, and use fields of the NameTree entry.
However, this is not ideal because NameTree is introduced to improve the performance of four tables, and it should not change the procedure of forwarding pipelines.

To reduce the number of index lookups, but still hide NameTree away from forwarding pipelines, we add shortcuts between tables.
Each FIB/PIT/Strategy~Choice/Measurements entry contains a pointer to the corresponding NameTree entry; the NameTree entry contains pointers to FIB/PIT/Strategy~Choice/Measurements entries and the parent NameTree entry.
Therefore, for example, given a PIT entry, one can retrieve the corresponding NameTree entry in constant time by following the pointer\footnote{This applies only if the PIT entry's Interest Name does not end with an implicit digest; otherwise, a regular lookup would be performed.}, and then retrieve or attach a Measurements entry via the NameTree entry, or find longest prefix match FIB entry by following pointers to parents.

NameTree entry is still exposed to forwarding if we take this approach.
To also hide NameTree entry away, we introduce new overloads to table algorithms that take a relevant table entry in place of a Name.
These overloads include:

\begin{itemize}[nosep]
\item \texttt{Fib::findLongestPrefixMatch} can accept PIT entry or Measurements entry in place of a Name
\item \texttt{StrategyChoice::findEffectiveStrategy} can accept PIT entry or Measurements entry in place of a Name
\item \texttt{Measurements::get} can accept FIB entry or PIT entry in place of a Name
\end{itemize}

An overload that takes a table entry is generally more efficient than the overload taking a Name.
Forwarding can take advantage of reduced index lookups by using those overloads, but does not need to deal with NameTree entry directly.

To support these overloads, NameTree provides \texttt{NameTree::getEntry} function template, which returns the NameTree entry linked from a FIB/PIT/Strategy~Choice/Measurements entry.
\texttt{NameTree::getEntry} allows one table to retrieve the corresponding NameTree from an entry of another table, without knowning the internal structure of that entry.
It also permits a table to depart from NameTree in the future without breaking other code: suppose someday Measurements is no longer based on NameTree, \texttt{NameTree::getEntry} could perform a lookup using Interest Name in the Measurements entry; \texttt{Fib::findLongestPrefixMatch} can still accept Measurements entries, although it's not more efficient than using a Name.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
