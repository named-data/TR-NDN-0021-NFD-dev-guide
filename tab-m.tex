\subsection{Measurements Table}\label{sec:tab-m}

The Measurements Table is used by forwarding strategies to store measurements information regarding a name prefix.
Strategy can store arbitrary information in PIT and in Measurements (Section~\ref{sec:strategy-storage}).
The Measurements Table is indexed by namespace, so it's suitable to store information that is associated with a namespace, but not specific to an Interest.

The structure and algorithms of the Measurements Table is outlined in Section~\ref{sec:tab-m-struct}.
How the Measurements Table is used by rest of NFD is described in Section~\ref{sec:tab-m-usage}.
The implementation of the Measurements Table algorithms is discussed in Section~\ref{sec:tab-nt}.


\subsubsection{Structure}\label{sec:tab-m-struct}

\paragraph{Measurements entry}\

A Measurements entry (\texttt{nfd::measurements::Entry}) contains a Name, and APIs for strategy to store and retrieve arbitrary information (\texttt{nfd::StrategyInfoHost}, Section~\ref{sec:strategy-storage}).
It's possible to add some standard metrics that can be shared among strategies, such as round trip time, delay, jitter, etc.
However, we feel that every strategy has its unique needs, and adding those standard metrics would become unnecessary overhead if the effective strategy is not making use of them.
Therefore, currently the Measurements entry does not contain standard metrics.

\paragraph{Measurements Table}\

The Measurements Table (\texttt{nfd::Measurements}) is a collection of Measurements entries.

\texttt{Measurements::get} method finds or inserts a Measurements entry.
The parameter is a Name, a FIB entry, or a PIT entry.
Because of how the Measurements Table is implemented, it's more efficient to pass in a FIB entry or a PIT entry, than to use a Name.
\texttt{Measurements::getParent} method finds or inserts a Measurements entry of the parent namespace.

Unlike other tables, there is no delete operation.
Instead, each entry has a limited lifetime and is automatically deleted when its lifetime expires.
A strategy must call \texttt{Measurements::extendLifetime} to request extending the lifetime of an entry.

Exact match and longest prefix match lookups are supported for retrieving existing entries.


\subsubsection{Usage}\label{sec:tab-m-usage}

The Measurements Table is solely used by forwarding strategy.
How many entries are in the Measurements Table and how often they are accessed are determined by forwarding strategies.
A well-written forwarding strategy stores no more than $O(log(N))$ entries, and performs no more than $O(N)$ lookups, where $N$ is the number of incoming packets plus the number of outgoing packets.

\paragraph{Measurements Accessor}\

Recall that NFD has per-namespace strategy choice (Section~\ref{sec:tab-sc}), each forwarding strategy is allowed to access the portion of the Measurements Table that are under the namespaces managed by that strategy.
This restriction is enforced by a Measurements Accessor.

A Measurements Accessor (\texttt{nfd::MeasurementsAccessor}) is a proxy for a strategy to access the Measurements Table.
Its APIs are similar to the Measurements Table.
Before returning any Measurements entry, the accessor looks up the Strategy Choice Table (Section~\ref{sec:tab-sc}) to confirm whether the requesting strategy owns the Measurements entry.
If an access violation is detected, null is returned instead of the entry.

