\section{RIB Management}
\label{sec:rib}

In NFD, the routing information base (RIB) stores static or dynamic routing information registered by applications, operators, and NFD itself.
Each piece of routing information is represented by a route and is associated with a particular namespace.
The RIB contains a list of \textbf{RIB entries}~\cite{RibMgmt} each of which maintains a list of \textbf{Routes}~\cite{RibMgmt} associated with the same namespace.
RIB entries form a RIB tree structure via a parent pointer and children pointers.

Routing information in the RIB is used to calculate next hops for FIB entries in the FIB (Section~\ref{sec:tab-fib}).
The RIB manager manages the RIB and updates the FIB when needed.
While a FIB entry can contain at most one NextHop record toward the same outgoing face, a RIB entry can contain multiple Routes toward the same outgoing face
since different applications may create routes to the same outgoing face each with different parameters.
These multiple Routes are processed by the RIB manager to calculate a single NextHop record toward an outgoing face for the FIB entry (Section~\ref{sec:prefix-registration-flags}).

The RIB manager is implemented as an independent module of NFD, which runs as a separate thread and communicates with NFD through a face.\footnote{On NFD-android platform (\url{https://github.com/named-data/NFD-android}), RIB manager runs within the same thread as NFD, but is still independent.}
This separation was done in order to keep packet forwarding logic lightweight and simple, as RIB operations can require complex manipulations.
Figure~\ref{fig:rib-arch} shows the high-level interaction of the RIB Manager with NFD and other applications.
A more detailed interaction is shown in Figure~\ref{fig:rib-timing}.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.8]{rib-arch}
\caption{RIB Manager---system diagram}\label{fig:rib-arch}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.8]{rib-timing}
\caption{RIB Manager---timing diagram}\label{fig:rib-timing}
\end{figure}


NFD provides various route inheritance flags for prefix registration that allow fine grained control and features such as hole-punching in a namespace.
Depending on the flag, a single registration request may result in multiple FIB entry changes.
The RIB manager takes the responsibility of processing these route inheritance flags in place of the FIB Manager.
In other words, the RIB manager receives all registration requests, processes the included route inheritance flags, and creates FIB updates as needed, which makes the forwarder leaner and faster.
To facilitate this FIB update calculation, a RIB entry stores a list of inherited routes that have been calculated by the FIB Updater (Section~\ref{sec:rib-fib-updater}) and applied to the entry's namespace.
Each RIB entry also keeps a count of the number of its routes which have the \texttt{CAPTURE} flag set (Section~\ref{sec:prefix-registration-flags}).
This list of inherited routes and the count of routes with their \texttt{CAPTURE} flag set are used by the FIB Updater to help calculate FIB updates.

The RIB manager provides an automatic prefix propagation feature which can be used to propagate knowledge of locally registered prefixes to a remote NFD.
When enabled, the Auto Prefix Propagator component handles these propagations~(Section \ref{sec:rib-propagation}).

As the RIB can be updated by different parties in different ways, including various routing protocols, application's prefix registrations, and command-line manipulation by sysadmins, the RIB manager provides a common interface for these processes and generates a consistent forwarding table.
These different parties use the RIB Manager's interface through \emph{control commands}~\cite{control-command}, which need to be validated for authentication and authorization.
The RIB manager listens for control commands with the command verbs \emph{register} and \emph{unregister} under the prefixes \emph{/localhost/nfd/rib} and \emph{/localhop/nfd/rib}~\cite{RibMgmt}.
The steps taken in processing these control commands are described in detail in Section~\ref{sec:rib-command-processing}.
Applications should use the RIB management interface to manipulate the RIB, and only the RIB manager should use the FIB management interface to directly manipulate NFD's FIB.

\subsection{Initializing the RIB Manager}

When an instance of the RIB Manager is created, the following operations are performed:
\begin{enumerate}
\item Command validation rules are loaded from the the \emph{rib} block of the NFD configuration file from the \emph{localhost\_security} and \emph{localhop\_security} subsections.
\item The Auto Prefix Propagator is initialized from the \emph{auto\_prefix\_propagate} subsection with values to set the forwarding cost of remotely registered prefixes, the timeout interval of a remote prefix registration command, how often propagations are refreshed, and the minimum and maximum wait time before retrying a propagation.
\item The control command prefixes \emph{/localhost/nfd/rib} and \emph{/localhop/nfd/rib} (if enabled) are ``self-registered'' in NFD's FIB.  This allows the RIB manager to receive RIB-related control commands (registration/unregistration requests) and requests for RIB management datasets.
\item The IncomingFaceId feature~\cite{NDNLPv2} is requested on the face between the RIB manager and NFD.
This allows the RIB manager to get the FaceId from where prefix registration/unregistration commands were received (for ``self-registrations'' from NDN applications).
\item The RIB manager subscribes to the face status notifications using the FaceMonitor class~\cite{ndn-cxx-guide} to receive notifications whenever a face is created or destroyed so that when a face is destroyed, Routes associated with that face can be deleted.
\end{enumerate}

\subsection{Command Processing}
\label{sec:rib-command-processing}
When the RIB manager receives a control command request, it first validates the Interest.
The RIB manager uses the command validation rules defined in the \emph{localhost\_security} and \emph{localhop\_security} sections of the \emph{rib} block in the NFD configuration file to determine if the signature on the command request is valid.
If the validation fails, it returns a control response with \texttt{error code 403}.
If the validation is successful, it confirms the passed command is valid and if it is, executes one of the following commands:

\begin{itemize}
 \item \textbf{\texttt{Register Route}}: The RIB Manager takes the passed parameters from the incoming request and uses them to create a RIB update.
If the FaceId is 0 or omitted in the command parameters, it means the requester wants to register a route to itself.
This is called a self-registration.
In this case, the RIB manager creates the RIB update with the requester's FaceId from the IncomingFaceId field.
The RIB Manager then passes the RIB update to the RIB to begin the FIB update process (Section~\ref{sec:rib-fib-updater}).
The FIB Updater is invoked and if the FIB update process is successful, the RIB searches
for a route that matches the name, FaceId, and Origin of the incoming request. If no match is
found, the passed parameters are inserted as a new route. Otherwise, the matching route is updated.
In both cases, the route is scheduled to expire after the passed expiration period and if the expiration period is being updated, the old expiration time is cancelled.
If the registration request creates a new RIB entry and automatic prefix propagation is enabled, the Auto Prefix Propagator's \texttt{afterInsertRibEntry} method is invoked.

 \item \textbf{\texttt{Unregister Route}}: The RIB Manager takes the passed parameters from the incoming request and creates a RIB update.
If the FaceId is 0 or omitted in the command parameters, it means the requester wants to unregister a route to itself.
This is called a self-unregistration.
In this case, the RIB manager creates the RIB update with the requester's FaceId from the IncomingFaceId field.
The RIB manager then passes the RIB update to the RIB to begin the FIB update process (Section~\ref{sec:rib-fib-updater}).
The FIB Updater is invoked and if the FIB update process is successful,
the Route with the same name, FaceId, and origin is removed from the RIB.
If automatic prefix propagation is enabled, the Auto Prefix Propagator's \texttt{afterEraseRibEntry} method is invoked.
\end{itemize}

In both cases, the RIB Manager returns a control response with \texttt{code 200} if the command is received successfully.
Because one RIB update may produce multiple FIB updates,
the RIB Manager does not return the result of the command execution since the application of multiple FIB updates may take longer than the \texttt{InterestLifetime} associated with the RIB update command.
Figure~\ref{fig:rib-dispatch} shows the verb dispatch workflow of the RIB manager.

If the expiration period in the \emph{register} command parameters is not set to infinity, the route will be scheduled to expire after the expiration period.
When the route expires, steps similar to an unregistration command are performed to remove the route, as well as any corresponding inherited routes, from the FIB and RIB.

\subsection{FIB Updater}
\label{sec:rib-fib-updater}

The FIB Updater is used by the RIB Manager to process route inheritance flags (Section~\ref{sec:prefix-registration-flags}), generate FIB updates, and
send FIB updates to NFD.
The RIB is only modified after FIB updates generated by a RIB update are applied by the FIB Updater successfully.
The RIB Manager takes parameters from incoming requests and creates a RIB update that is passed to the RIB
using the \texttt{Rib::beginApplyUpdate} method.
The RIB creates a RibUpdateBatch, a collection of RIB updates for the same FaceId, and adds the batch to a queue.
The RIB will advance the queue and hand off each batch to the FIB Updater for processing.
Only one batch can be handled by the FIB Updater at a time, so the RIB will only advance the queue when the FIB Updater
is not busy. The FIB Updater processes the route inheritance flags of the update in the received RibUpdateBatch,
generates the necessary FIB updates, and sends the FIB updates to the FIB as needed.
If the FIB updates are applied successfully, the RIB update is applied to the RIB.
Otherwise, the FIB update process fails, and the RIB update is not applied to the RIB.

If a FIB update fails:

\begin{enumerate}
  \item in case of a non-recoverable error (eg. signing key of the RIB Manager is not trusted, more than 10 timeouts for the same command), NFD will terminate with an error;
  \item in case of a non-existent face error with the same face as the RibUpdateBatch, the RibUpdateBatch is abandoned;
  \item in case of a non-existent face error with a different face than the RibUpdateBatch, the FIB update that failed is skipped;
  \item in other cases (eg. timeout), the FIB update is retried.
\end{enumerate}

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.6]{rib-dispatch}
\caption{Verb dispatch workflow of the RIB Manager}\label{fig:rib-dispatch}
\end{figure}

\subsubsection{Route Inheritance Flags}
\label{sec:prefix-registration-flags}

The route inheritance flags allow fine grained control over prefix registration.
The currently defined flags and examples of route inheritance can be found at~\cite{RibMgmt}.

\subsubsection{Cost Inheritance}
\label{sec:rib-cost-inheritence}

Currently, NFD implements the following logic to assign costs to next hops in the FIB when \texttt{CHILD\_INHERIT} is set.
When the flag is set on the route of a RIB entry, that route's face and cost are applied to the longer prefixes (children)
under the RIB entry's namespace. If a child already has a route with the face that would be inherited, the inherited route's face and cost are \emph{not}
applied to that child's next hops in the FIB. Also, if a child already has a route with the face that would be inherited and the child's route has its
\texttt{CHILD\_INHERIT} flag set, the inherited route's face and cost are \emph{not} applied to the next hops of the child nor the children of the child namespace.
If a RIB entry has neither the \texttt{CHILD\_INHERIT} nor the \texttt{CAPTURE} flag set on any of its routes, that RIB entry can inherit routes from
longer prefixes which do not have the same FaceId as one of the RIB entry's routes.
Examples of the currently implemented logic are shown in Table~\ref{tab:sample-rib-fib}.

Future versions will assign the lowest available cost to a next hop face based on all inherited RIB entries not blocked by a \texttt{CAPTURE} flag.
Examples of the future logic are shown at~\cite{RibMgmt}.

\begin{table}[ht]
\caption{Nexthop cost calculation. The nexthops and their costs for each RIB entry are calculated using the RIB (the table on the left)
              and are installed into the FIB (the table on the right).}
\label{tab:sample-rib-fib}
\begin{minipage}{0.6\textwidth}
\begin{tabular}{| l | c | c | c | c |}
\hline
  Name prefix & Nexthop FaceId & \texttt{CHILD\_INHERIT} & \texttt{CAPTURE} & Cost\\\hline
  / & 1 & \texttt{true} & \texttt{false} & 75\\ \hline
  /a & 2 & \texttt{false} & \texttt{false} & 50\\ \hline
  /a/b & 1 & \texttt{false} & \texttt{false} & 65\\ \hline
  /b & 1 & \texttt{true} & \texttt{false} & 100\\ \hline
  /b/c & 3 & \texttt{true} & \texttt{true} & 40\\ \hline
  /b/c/e & 1 & \texttt{false} & \texttt{false} & 15\\ \hline
  /b/d & 4 & \texttt{false} & \texttt{false} & 30\\ \hline
\end{tabular}
\label{tab:sample-rib-cost}
\end{minipage}
\begin{minipage}{0.6\textwidth}
\begin{tabular}{| l | c |}
\hline
  Name prefix & (Nexthop FaceId, Cost)\\\hline
  / & (1, 75)\\ \hline
  /a & (2, 50), (1, 75)\\ \hline
  /a/b & (1, 65)\\ \hline
  /b & (1, 100)\\ \hline
  /b/c & (3, 40)\\ \hline
  /b/c/e & (1, 15), (3, 40)\\ \hline
  /b/d & (4, 30), (1, 100)\\ \hline
\end{tabular}
\end{minipage}
\end{table}

\subsection{RIB Status Dataset }
\label{sec:rib-status-dataset}

The RIB manager publishes a status dataset under the prefix \emph{/localhost/nfd/rib/list} which contains the current contents of the RIB.
When the RIB Manager receives an Interest under the dataset prefix, the \texttt{listEntries} method is called.
The RIB is serialized in the form of a collection of RibEntry and nested Route TLVs~\cite{RibMgmt} and the dataset is published.

\subsection{Readvertise}
\label{sec:readvertise}

The Readvertise module provides a system where some policy can decide
whether a route from the local RIB should be readvertised to some
destination. It is triggered whenever the contents of the RIB
change. Each given \texttt{Readvertise} instance has exactly one
\texttt{ReadvertisePolicy} instance and one
\texttt{ReadvertiseDestination} instance. Since Readvertise is a
platform that can model several kinds of prefix propagation scenarios,
a separate Readvertise instance must be maintained for each
scenario. Currently NFD contains only one instance, used to
communicate changes in the contents of the RIB to NLSR.

\begin{figure}[h]
\centering
\includegraphics{readvertise}
\caption{The simplified structure an operation of the Readvertise module.}\label{readvertise-diagram}
\end{figure}

The Readvertise module subscribes to \texttt{Signals} that the RIB
maintains, which are signaled to after a RIB route is added and
before one is removed. On route addition, Readvertise consults its
\texttt{ReadvertisePolicy} instance. If the policy chooses to
readvertise a prefix, which may not be exactly the prefix associated
with the RIB route, it will ask Readvertise to advertise the prefix to
the destination, which will create a \texttt{ReadvertisedRoute} object
if one does not exist, and increment the count of how many RIB routes
advertise the prefix. How the destination actually models this is an
implementation detail. For example, the Readvertise-to-NLSR policy and
destination pair uses RibMgmt commands sent to a prefix that the local
NLSR is listening on to communicate advertisements and withdrawals of
routes. On route removal, Readvertise will check to see if any other
RIB routes advertise this prefix. If not, it will call the
destination's withdraw function. On successful withdrawal, the
\texttt{ReadvertisedRoute} will be deleted. A failure from either an
advertisement or withdrawal will prompt retries with exponential
backoff.

If at any time the destination becomes unavailable, Readvertise will
wait until next the destination is next available to advertise or
withdraw any \texttt{ReadvertisedRoute}s as needed. Destination
availability is left as an implementation detail, to be decided
case-by-case by the destination implementer.

\subsubsection{ReadvertisePolicy}
When Readvertise receives a \texttt{Signal} that a RIB route has been
added, it will call the \texttt{handleNewRoute()} function. The
\texttt{ReadvertisePolicy} defines what kinds of prefixes should be
advertised, specifically which prefix to advertise and what signing
credentials to sign those advertised prefixes with. For example, in
the Readvertise-to-NLSR case, the advertised prefix is just the prefix
associated with the RIB route that triggered the
advertisement. However, a policy could choose to advertise any prefix,
perhaps the shortest sub-prefix that it can sign with some keychain.

Each policy must also define a refresh interval that specifies how
often successfully advertised prefixes should be refreshed at the
destination.

\subsubsection{ReadvertiseDestination}
As stated above, if the \texttt{ReadvertisePolicy} chooses to
advertise a prefix, it will provide Readvertise with a prefix and
signing credentials to advertise. Then Readvertise will call the
\texttt{advertise()} function. How the function works
is left as an implementation detail per-destination. Additionally,
each destination must define the \texttt{withdraw()} function and
should have some mechanism to manage its availability. The semantics
of a destination's availability are left for the implementer to
decide, keeping in mind that Readvertise will not attempt to advertise
or withdraw prefixes while the destination is unavailable. Whenever a
destination's availability changes, that destination should emit a
\texttt{Signal} to alert Readvertise.

\subsection{Auto Prefix Propagator}\label{sec:rib-propagation}

The Auto Prefix Propagator can be enabled by the RIB manager to
propagate necessary knowledge of local prefix registrations to a single
connected gateway router.
Since gateway routers are currently configured to accept prefix registration commands under \textit{/localhop/nfd/rib},
the Auto Prefix Propagator cannot handle connections to multiple gateway routers;
the \textit{/localhop/nfd/rib} prefix does not allow the Auto Prefix Propagator to distinguish which gateway router the propagations should and should not be forwarded to.
Figure~\ref{fig:rib-propagation} provides an example of a locally registered prefix being propagated to a connected gateway router.
The Auto Prefix Propagator \emph{propagates} prefixes by registering prefixes with a remote NFD and \emph{revokes} prefixes by unregistering prefixes from a remote NFD.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.6]{rib-propagation}
\caption{A local prefix registration is propagated to a connected gateway router}\label{fig:rib-propagation}
\end{figure}

\subsubsection{What Prefix to Propagate}
To reduce not only the cost of propagations but also changes to the
router's RIB, the propagated prefixes should be aggregated whenever
possible. The local RIB Manager owns a key-chain consisting
of a set of identities, each of which defines a namespace and can
cover one or more local RIB entries. Given a RIB entry, the Auto
Prefix Propagator queries the local key-chain for signing identities
that are authorized to sign a prefix registration command for a prefix
of the RIB prefix. If one or more signing identities are found, the
identity with the shortest prefix that can sign a prefix registration command
is chosen, and the Auto Prefix Propagator will then attempt to
propagate that shortest prefix of the prefix to the
router. Figure~\ref{fig:propagation} presents a high level example of
prefix propagation.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.6\textwidth]{app-what-to-propagate}
\caption{An example of prefix propagation}\label{fig:propagation}
\end{figure}

If the propagation succeeds, an event is scheduled to propagate the
same prefix to refresh this propagation after a pre-defined duration. By contrast, if the
propagation fails, another event is scheduled to propagate the same
prefix to retry this propagation after some waiting period that is
calculated based on an exponential back-off strategy.

\subsubsection{When to Propagate}
The Auto Prefix Propagator monitors RIB insertions and deletions by
subscribing to two signals, \texttt{Rib:afterInsertEntry} and
\texttt{Rib:afterEraseEntry} respectively. Once those two signals are
emitted, two connecting (the connections are established when Auto
Prefix Propagator is enabled) methods,
\texttt{AutoPrefixPropagator::afterInsertRibEntry} and \\
\texttt{AutoPrefixPropagator::afterEraseRibEntry}, are invoked
to process the insertion or deletion, respectively.

When an insertion is processed, the Auto Prefix Propagator will not attempt to propagate prefixes scoped for local use (i.e., starts with \texttt{/localhost}) or that indicate connectivity to a router (i.e., the \textbf{link local NFD prefix}).
The Auto Prefix Propagator also requires an active connection to the gateway router before attempting a propagation.
The Auto Prefix Propagator considers the connection to the router as active if the local RIB has the \textit{link local NFD prefix} registered and inactive if the local RIB does not have the \textit{link local NFD prefix} registered.
If the prefix has not been propagated previously, a propagation attempt will be made for the prefix.

Similarly, a revocation after deletion requires a qualified RIB prefix (not starting with \texttt{/localhost} or the \textbf{link local NFD prefix}),
an active connection to a gateway router,
that the prefix has been propagated previously, and that no other existing RIB prefix caused a propagation for the same prefix.

Figure~\ref{fig:workflow} demonstrates a simplified workflow of a
successful propagation. The process for revocation is similar.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.9\textwidth]{app-work-flow}
\caption{The simplified workflow of a successful propagation}\label{fig:workflow}
\end{figure}

\subsubsection{Secure Propagations}
\label{sec:secure-register}
To enable gateway routers to process remote registrations, the
\texttt{rib.localhop-security} section must be configured on the router. A series of
polices and rules can be defined to validate registration/unregistration
commands for propagations/revocations.

According to the trust schema, a command for
propagation/revocation cannot pass the validation unless its signing identity can be
verified by the configured trust anchor on the gateway router.

\subsubsection{Propagated-entry State Machine}
A propagated entry consists of a \texttt{PropagationStatus} which indicates the current state of the entry, as well as a scheduled event for either a refresh or retry. In addition, it stores a copy of the signing identity for the entry. All propagated entries are maintained as an unordered map (\texttt{AutoPrefixPropagator::m\_propagatedEntries}), where the propagated prefix is used as the key to retrieve the corresponding entry.

More specifically, a propagated entry will stay in one of the following five states in logic.

\begin{itemize}
\item \texttt{NEW}, the initial state.
\item \texttt{PROPAGATING}, the state when the corresponding propagation is being processed but the response is not back yet.
\item \texttt{PROPAGATED}, the state when the corresponding propagation has succeeded.
\item \texttt{PROPAGATE\_FAIL}, the state when the corresponding propagation has failed.
\item \texttt{RELEASED}, indicates this entry has been released. It's noteworthy that this state is not required to be explicitly implemented, because it can be easily determined by checking whether an existing entry can still be accessed. Thus, any entry to be released is directly erased from the list of propagated entries.
\end{itemize}

Given a propagated entry, there are a series of events that can lead to a transition with a state switch from one to another, or some triggered actions, or even both. All related input events are listed below.

\begin{itemize}
\item \texttt{rib insert}, corresponds to \texttt{AutoPrefixPropagator::afterRibInsert}, which happens when the insertion of a RIB entry triggers a necessary propagation.
\item \texttt{rib erase}, corresponds to \texttt{AutoPrefixPropagator::afterRibErase}, which happens when the deletion of a RIB entry triggers a necessary revocation.
\item \texttt{hub connect}, corresponds to \texttt{AutoPrefixPropagator::afterHubConnect}, which happens when the connectivity to a router is established (or recovered).
\item \texttt{hub disconnect}, corresponds to \texttt{AutoPrefixPropagator::afterHubDisconnect}, which happens when the connectivity to the router is lost.
\item \texttt{propagate succeed}, corresponds to \texttt{AutoPrefixPropagator::afterPropagateSucceed}, which happens when the propagation succeeds on the router.
\item \texttt{propagate fail}, corresponds to \texttt{AutoPrefixPropagator::afterPropagateFail}, which happens when a failure is reported in response to the registration command for propagation.
\item \texttt{revoke succeed}, corresponds to \texttt{AutoPrefixPropagator::afterRevokeSucceed}, which happens when the revocation of some propagation succeeds on the router.
\item \texttt{revoke fail}, corresponds to \texttt{AutoPrefixPropagator::afterRevokeFail}, which happens when a failure is reported in response to the unregistration command for revocation.
\item \texttt{refresh timer}, corresponds to \texttt{AutoPrefixPropagator::onRefreshTimer}, which happens when the timer scheduled to refresh some propagation is fired.
\item \texttt{retry timer}, corresponds to \texttt{AutoPrefixPropagator::onRetryTimer}, which happens when the timer scheduled to retry some propagation is fired.
\end{itemize}

A state machine is implemented to maintain and direct transitions according to the input events. Figure~\ref{fig:transition:table} lists all related events and the corresponding transitions.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.9\textwidth]{app-transition-table}
\caption{\label{fig:transition:table} The transition table of propagated-entry state machine.}
\end{figure}

\subsubsection{Configure Auto Prefix Propagator}

When the RIB manager loads configurations from the \texttt{rib} section in the NFD configuration file, the Auto Prefix Propagator will load
its configurations from the sub section
\texttt{rib.auto\_prefix\_propagate}.

Table~\ref{tb:parameters} presents the common parameters shared by all propagations and which parameters are configurable.

\begin{table}[htbp]
\caption{\label{tb:parameters} Shared parameters for prefix propagation}

\begin{minipage}{\textwidth}
\centering
\renewcommand{\arraystretch}{1.2}

\begin{tabular}{|c|c|c|c|}\hline
\multicolumn{2}{|c|}{\textbf{member variable\footnote{%
these parameters are maintained in some member variables of \textbf{AutoPrefixPropagator}.}}}
& \textbf{default setting}
& \textbf{configurable\footnote{%
indicates whether this parameter can be configured in the NFD config file.}}\\ \hline
%
\multirow{3}*{\texttt{m\_controlParameters}} & Cost & 15 & YES \\ \cline{2-4}
& Origin & ndn::nfd::ROUTE\_ORIGIN\_CLIENT & NO \\ \cline{2-4}
& FaceId & 0 & NO \\\hline
%
\multirow{2}*{\texttt{m\_commandOptions}} & Prefix & \texttt{/localhop/nfd} & NO \\ \cline{2-4}
& Timeout & 10,000 (milliseconds) & YES \\ \hline
%
\multicolumn{2}{|c|}{\texttt{m\_refreshInterval\footnote{%
this setting must be less than the idle time of UDP faces whose default setting is 3,600 seconds.}}}
& 25 (seconds) & YES \\ \hline
%
\multicolumn{2}{|c|}{\texttt{m\_baseRetryWait}} & 50 (seconds) & YES \\ \hline
\multicolumn{2}{|c|}{\texttt{m\_maxRetryWait}} & 3600 (seconds) & YES \\ \hline
%
\end{tabular}
\end{minipage}

\end{table}

\subsection{Extending RIB Manager}

The RIB Manager currently supports two commands (register and unregister), RIB dataset publishing, and the automatic prefix propagation feature.
However, the functionality of the RIB Manager can be extended by introducing more commands and features.
For example, in the current implementation, if a node wants to announce a prefix, it needs to communicate with a specific routing protocol.
Once the RIB manager defines an interface for prefix announcement, e.g., advertise and withdraw commands, the process of announcing and withdrawing prefixes in routing protocols could become more uniform and simple.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
