\section{Introduction}\label{sec:intro}

NDN Forwarding Daemon (NFD) is a network forwarder that implements and evolves together with the Named Data Networking (NDN) protocol~\cite{ndn-tlv}.
This document explains the internals of NFD and is intended for developers who are interested in extending and improving NFD.
Other information about NFD, including instructions of how to compile and run NFD, are available on NFD's home page~\cite{nfd}.

The main design goal of NFD is to support diverse experimentation with NDN architecture.
The design emphasizes \emph{modularity} and \emph{extensibility} to allow easy experiments with new protocol features, algorithms, and applications.
We have not fully optimized the code for performance.
The intention is that performance optimizations are one type of experiments that developers can conduct by trying out different data structures and different algorithms; over time, better implementations may emerge within the same design framework.

NFD will keep evolving in three aspects: improvement of the modularity framework, keeping up with the NDN protocol spec, and addition of new features.
We hope to keep the modular framework stable and lean, allowing researchers to implement and experiment with various features, some of which may eventually work into the protocol specification.

%The design and development of NFD benefited from our earlier experience with CCNx~\cite{ccnx} software package.
%However, NFD is not in any part derived from CCNx codebase and does not maintain compatibility with CCNx.


\subsection{NFD Modules}\label{sec:intro-modules}

The main functionality of NFD is to forward Interest and Data packets.
To do this, it abstracts lower-level network transport mechanisms into NDN Faces, maintains basic data structures like CS, PIT, and FIB, and implements the packet processing logic.
In addition to basic packet forwarding, it also supports multiple forwarding strategies, and a management interface to configure, control, and monitor NFD.
As illustrated in Figure~\ref{fig:nfd-overview}, NFD contains the following inter-dependent modules:

\begin{figure}[b!]
\centering
\includegraphics[scale=0.6]{modules-overview}
\caption{Overview of NFD modules}
\label{fig:nfd-overview}
\end{figure}

\begin{itemize}

\item \textbf{ndn-cxx Library, Core, and Tools} (Section~\ref{sec:core})

Provides various common services shared between different NFD modules.
These include hash computation routines, DNS resolver, config file, Face monitoring, and several other modules.

\item \textbf{Faces} (Section~\ref{sec:face})

Implements the NDN Face abstraction on top of various lower level transport mechanisms.

\item \textbf{Tables} (Section~\ref{sec:tables})

Implements the Content Store (CS), the Interest table (PIT), the Forwarding Information Base (FIB), Stra\-te\-gy~Cho\-ice, Measurements, and other data structures to support forwarding of NDN Data and Interest packets.

%The Strategy Choice records the choice of the strategy for a namespace, and Measurement records are used by strategies to store past performance results for namespaces.

\item \textbf{Forwarding} (Section~\ref{sec:fw})

Implements basic packet processing pathways, which interact with Faces, Tables, and Strategies (Section~\ref{sec:strategy}).

Strategies are a major part of the forwarding module. The forwarding module implements a framework to support different forwarding strategies in the form of forwarding pipelines, described in detail in Section~\ref{sec:fw}.

\item \textbf{Management} (Section~\ref{sec:mgmt})

Implements the NFD Management Protocol~\cite{nfd-management}, which allows applications to configure NFD and set/query NFD's internal states.  Protocol interaction is done via NDN's Interest/Data exchange between applications and NFD.

\item \textbf{RIB Management} (Section~\ref{sec:rib})

Manages the routing information base (RIB).
The RIB may be updated by different parties in different ways, including various routing protocols, application prefix registrations, and command-line manipulation by sysadmins.
The RIB management module processes all these requests to generate a consistent forwarding table, and syncs it up with NFD's FIB, which contains only the minimal information needed for forwarding decisions.

\end{itemize}

The rest of this document will explain all these modules in more detail.


\subsection{How Packets are Processed in NFD}\label{sec:intro-packet}

To give readers a better idea on how NFD works, this section explains how a packet is processed in NFD.

Packets arrive at NFD via \textit{Faces}.
``Face'' is a generalization of ``interface''.
It can be either a physical interface (where NDN operates directly over Ethernet), or an overlay tunnel (where NDN operates as an overlay above TCP, UDP, or WebSocket).
In addition, communication between NFD and a local application can be done via a Unix-domain socket, which is also a Face.
A Face is composed of a \textit{LinkService} and a \textit{Transport}. The \textit{LinkService} provides high-level services for the Face, like fragmentation and reassembly, network-layer counters, and failure detection, while the \textit{Transport} acts as an wrapper over an underlying network transmission protocol (TCP, UDP, Ethernet, etc.) and provides services like link-layer counters.
The Face reads incoming stream or datagrams via the operating system API, extracts network-layer packets from link protocol packets, and delivers these network-layer packets (NDN packet format Interests, Datas, or Nacks) to the forwarding.

A network-layer packet (Interest, Data, or Nack) is processed by \textit{forwarding pipelines}, which define series of steps that operate on the packet.
NFD's data plane is stateful, and what NFD does to a packet depends on not only the packet itself but also the forwarding state, which is stored in \textit{tables}.

When the forwarder receives an Interest packet, the incoming Interest is first inserted into the \textit{Interest table} (PIT), where each entry represents a pending Interest or a recently satisfied Interest.
A lookup for a matching Data is performed on the \textit{Content Store} (CS), which is an in-network cache of Data packets.
If there is a matching Data packet in CS, that Data packet is returned to the requester; otherwise, the Interest needs to be forwarded.

A \textit{forwarding strategy} decides how to forward an Interest.
NFD allows per-namespace strategy choice; to decide which strategy is responsible for forwarding an Interest, a longest prefix match lookup is performed on the \textit{Strategy Choice table}, which contains strategy configuration.
The strategy responsible for an Interest (or, more precisely, the PIT entry) makes a decision whether, when, and where to forward the Interest.
While making this decision, the strategy can take input from the \textit{Forwarding Information Base} (FIB), which contains routing information that comes from local application's prefix registrations and routing protocols, use strategy-specific information stored in the PIT entry, and record and use data plane performance measurements in Measurements entry.

After the strategy decides to forward an Interest to a specified Face, the Interest goes through a few more steps in forwarding pipelines, and then it is passed to the Face.
The Face, depending on the underlying protocol, fragments the Interest if necessary, encapsulates the network-layer packet(s) in one or more link-layer packets, and sends the link-layer packets as an outgoing stream or datagrams via the operating system APIs.

An incoming Data is processed differently.
Its first step is checking the Interest table to see if there are PIT entries that can be satisfied by this Data packet.
All matched entries are then selected for further processing.
If this Data can satisfy none of the PIT entries, it is unsolicited and it is dropped.
Otherwise, the Data is added to the Content Store.
Forwarding strategy that is responsible for each of the matched PIT entries is notified.
Through this notification, and another ``no Data comes back'' timeout, the strategy is able to observe the reachability and performance of paths; the strategy can remember its observations in the \textit{Measurements table}, in order to improve its future decisions.
Finally, the Data is sent to all requesters, recorded in downstream records (in-records) of the PIT entries; the process of sending a Data via a Face is similar to sending an Interest.

When the forwarder receives a Nack, the processing varies depending upon the \textit{forwarding strategy} in use.


\subsection{How Management Interests are Processed in NFD}\label{sec:intro-mgmt}

% One class of Interests are for management purposes.
NFD Management protocol~\cite{nfd-management} defines three inter-process management mechanisms that are based on Interest-Data exchanges: control commands, status datasets, and notification streams.
This section gives a brief overview how these mechanisms work and what are their requirements.

% A management Interest usually comes from a local application; a special case is that RIB management Interests can come from a remote peer.
% The processing in Face, forwarding pipelines, and forwarding strategy is same as a regular Interest.
% This implies that management Interests can be satisfied from the Content Store, if a matching Data packet exists.

A \textbf{control command} is a signed (authenticated) Interest to perform a state change within NFD.
Since the objective of each control command Interest is to reach the destination management module and not be satisfied from CS, each control command Interest is made unique through the use of timestamp and nonce components.
For more detail refer to control command specification~\cite{control-command}.

When NFD receives a control command request, it directs that request to a special Face, called the \textit{Internal Face}.%
\footnote{There is always a FIB entry for the management protocol prefix that points to the Internal Face.}
When a request is forwarded to this Face, it is dispatched internally to a designated \textit{manager} (e.g., Interests under \path{/localhost/nfd/faces} are dispatched to the Face manager, see Section~\ref{sec:mgmt}).
The manager then looks at the request name to decide which action is requested.
If the name refers to a valid control command, the dispatcher validates the command (checks the signature and validates whether the requester is authorized to send this command), and the manager performs the requested action if validation succeeds.
The response is sent back to the requester as a Data packet, which is processed by forwarding and Face in the same way as a regular Data.

The exception from the above procedure is RIB Management (Section~\ref{sec:rib}), which is performed in a separate thread.
All RIB Management control commands, instead of Internal Face, are forwarded toward the RIB thread using the same methods as forwarding to any local application (the RIB thread ``registers'' itself with NFD for the RIB management prefix when it starts).

% Internally, the RIB daemon makes use of FIB Management commands
% to synchronize RIB updates to the FIB.

A \textbf{status dataset} is a dataset containing an internal NFD status that is generated either periodically or on-demand (e.g., NFD general status or NFD Face status).
These datasets can be requested by anybody using a simple unsigned Interest directed towards the specific management module, as defined in the specification~\cite{nfd-management}.
An Interest requesting a new version of a \textbf{status dataset} is forwarded to the internal Face and then the designated manager the same way as control commands.
The manager, however, will not validate this Interest, but instead generate all segments of the requested dataset and put them into the forwarding pipeline.
This way, the first segment of the dataset will directly satisfy the initial Interest, while others will satisfy the subsequent Interests through CS.
In the unlikely event when subsequent segments are evicted from the CS before being fetched, the requester is responsible for restarting the fetch process from the beginning.

\textbf{Notification streams} are similar to status datasets in that they can be accessed by anybody using unsigned Interests, but operate differently.
Subscribers that want to receive notification streams still send Interests directed towards the designated manager.
However, these Interests are dropped by the dispatcher and are not forwarded to the manager.
Instead, whenever a notification is generated, the manager puts a Data packet into the forwarding, satisfying all outstanding notification stream Interests, and the notification is delivered to all subscribers.
It is expected that these Interests will not be satisfied immediately, and the subscribers are expected to re-express the notification stream Interests when they expire.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
