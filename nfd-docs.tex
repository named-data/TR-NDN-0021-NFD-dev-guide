\documentclass[10pt]{article}

\usepackage[margin=0.5in,bottom=.7in,top=.8in]{geometry}
\usepackage{graphicx}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{hyperref}
\usepackage{authblk}
\usepackage{cite}
\usepackage[inline,shortlabels]{enumitem}
\usepackage{multirow}

\graphicspath{{figures/}}
\hypersetup{
  breaklinks=true,
  colorlinks=true,
  citecolor=Mahogany,
  filecolor=Mahogany,
  linkcolor=Mahogany,
  urlcolor=Mahogany
}

\title{NFD Developer's Guide}

\author[1]{\mbox{Alexander Afanasyev}}
\author[2]{\mbox{Junxiao Shi}}
\author[2]{\mbox{Beichuan Zhang}}
\author[1]{\mbox{Lixia Zhang}}
\author[1]{\mbox{Ilya Moiseenko}}
\author[1]{\mbox{Yingdi Yu}}
\author[1]{\mbox{Wentao Shang}}
\author[1]{\mbox{Yanbiao Li}}
\author[1]{\mbox{Spyridon Mastorakis}}
\author[1,2]{\mbox{Eric Newberry}}
\author[2]{\mbox{Yi Huang}}
\author[2]{\mbox{Jerald Paul Abraham}}
\author[2]{\mbox{Teng Liang}}
\author[2]{\mbox{Klaus Schneider}}
\author[3]{\mbox{Steve DiBenedetto}}
\author[3]{\mbox{Chengyu Fan}}
\author[3]{\mbox{Susmit Shannigrahi}}
\author[3]{\mbox{Christos Papadopoulos}}
\author[4]{\mbox{Davide Pesavento}}
\author[4]{\mbox{Giulio Grassi}}
\author[4]{\mbox{Giovanni Pau}}
\author[5]{\mbox{Hang Zhang}}
\author[5]{\mbox{Tian Song}}
\author[6]{\mbox{Haowei Yuan}}
\author[6]{\mbox{Hila Ben Abraham}}
\author[6]{\mbox{Patrick Crowley}}
\author[7]{\mbox{Syed Obaid Amin}}
\author[7]{\mbox{Vince Lehman}}
\author[7]{\mbox{Muktadir Chowdhury}}
\author[7]{\mbox{Ashlesh Gawande}}
\author[7]{\mbox{Lan Wang}}
\author[7]{\mbox{Nicholas Gordon}}

\affil[1]{University of California, Los Angeles}
\affil[2]{The University of Arizona}
\affil[3]{Colorado State University}
\affil[4]{University Pierre \& Marie Curie, Sorbonne University}
\affil[5]{Beijing Institute of Technology}
\affil[6]{Washington University in St.\ Louis}
\affil[7]{The University of Memphis}

\date{NFD Team}

\usepackage{fancyhdr}
\pagestyle{fancy}


\usepackage{eso-pic,xcolor}
\makeatletter
\AddToShipoutPicture*{%
\setlength{\@tempdimb}{20pt}%
\setlength{\@tempdimc}{\paperheight}%
\setlength{\unitlength}{1pt}%
\put(\strip@pt\@tempdimb,\strip@pt\@tempdimc){%
    \makebox(0,-60)[l]{\color{blue}%
NDN, Technical Report NDN-0021. \url{https://named-data.net/publications/techreports/}}
  }%
}
\makeatother


\begin{document}

\maketitle

\begin{abstract}
NDN Forwarding Daemon~(NFD) is a network forwarder that implements the Named Data Networking~(NDN) protocol.
NFD is designed with \emph{modularity} and \emph{extensibility} in mind to enable easy experiments with new protocol features, algorithms, and applications for NDN.
To help developers extend and improve NFD, this document explains NFD's internals including the overall design, major modules, their implementations, and their interactions.
\end{abstract}

\section*{Revision history}

\begin{itemize}

\item \textbf{Revision 12 (TBD)}:
  \begin{itemize}[nosep,after=\strut]
  \item Miscellaneous updates to match to the latest NFD release (work in progress)
  \end{itemize}

\item \textbf{Revision 11 (July 30, 2021)}:
  \begin{itemize}[nosep,after=\strut]
  \item Added HopLimit to incoming and outgoing Interest processing pipelines
  \item Updates regarding the CongestionMark field
  \item IPv6 addresses in whitelist/blacklist are supported now
  \item Expanded the GenericLinkService feature table
  \item Updated description of strategy actions
  \item Vastly improved documentation of ASF strategy, imported from the ASF TR appendix
  \item Removed NCC strategy description (a legacy strategy that was removed from NFD)
  \item Miscellaneous updates to match to the latest NFD release (still incomplete)
  \end{itemize}

\item \textbf{Revision 10 (July 4, 2018)}:
  \begin{itemize}[nosep,after=\strut]
  \item Added description of UnicastEthernetTransport
  \item Updates regarding Transport MTU handling
  \end{itemize}

\item \textbf{Revision 9 (May 4, 2018)}:
  \begin{itemize}[nosep,after=\strut]
  \item Updated description of congestion control
  \item Miscellaneous updates in forwarding pipelines and strategy description
  \end{itemize}

\item \textbf{Revision 8 (February 19, 2018)}:
  \begin{itemize}[nosep,after=\strut]
  \item Updated description of face system
  \item Interface whitelist and blacklist for multicast faces
  \item TCP permanent face
  \item IPv6 support in MulticastUdpTransport
  \item New \emph{ad hoc} link type
  \item New Content Store policy configuration and policy API
  \item Added unsolicited data policy
  \item Forwarding pipeline updates, including semantics of removing Link from Interest when it reaches producer region
  \item Description of new semantics of NextHopFaceId
  \item Scope control in strategies
  \item Strategy parameters
  \item Updated description of multicast strategy
  \item Added description of Command Authenticator
  \item Updated face management to match current NFD implementation
  \item Added RIB-to-NLSR readvertise
  \item New section on congestion control
  \end{itemize}

\item \textbf{Revision 7 (October 4, 2016)}:
  \begin{itemize}[nosep,after=\strut]
  \item Added brief description and reference to the new Adaptive SRTT-based (ASF) forwarding strategy
  \item Update description of Strategy API to reflect latest changes
  \item Miscellaneous updates
  \end{itemize}

\item \textbf{Revision 6 (March 25, 2016)}:
  \begin{itemize}[nosep,after=\strut]
  \item Added description of refactored Face system (Face, LinkService, Transport)
  \item Added description of WebSocket transport
  \item Updated description of RIB management
  \item Added description of Nack processing
  \item Added introductory description of NDNLP
  \item Added description of best-route retransmission suppression
  \item Other updates to synchronize description with current NFD implementation
  \end{itemize}

\item \textbf{Revision 5 (Oct 27, 2015)}:
  \begin{itemize}[nosep,after=\strut]
  \item Added description of the Content Store CachePolicy API, including information about a new LRU policy
  \item BroadcastStrategy renamed to MulticastStrategy
  \item Added overview of how forwarder processes Link objects
  \item Added overview of the new face system (incomplete)
  \item Added description of the new automatic prefix propagation feature
  \item Added description of the refactored management
  \item Added description of NetworkRegionTable configuration
  \item Added description about \texttt{client.conf} and NFD
  \end{itemize}

\item \textbf{Revision 4 (May 12, 2015)}:
  New section about testing and updates for NFD version 0.3.2:
  \begin{itemize}[nosep,after=\strut]
  \item Added description of the new Content Store implementation, including the new async lookup model
  \item Added description of the remote prefix registration
  \item Updated ``Common Services'' section
  \end{itemize}

\item \textbf{Revision 3 (February 3, 2015)}:
  Updates for NFD version 0.3.0:
  \begin{itemize}[nosep,after=\strut]
  \item In Strategy interface, \texttt{beforeSatisfyPendingInterest} was renamed to \texttt{beforeSatisfyInterest}
  \item Added description of dead nonce list and related changes to forwarding pipelines
  \item Added description of a new \verb|strategy_choice| config file subsection
  \item Amended \texttt{unix} config text to reflect the removal of the \texttt{listen} option
  \item Added discussion about encapsulation of NDN packets inside WebSocket messages
  \item Revised FaceManager description, requiring canonical FaceUri in create operations
  \item Added description of the new access router strategy
  \end{itemize}

\item \textbf{Revision 2 (August 25, 2014)}:
  Updated steps in forwarding pipelines, \texttt{nfd::BestRouteStrategy} is replaced with \texttt{nfd::BestRouteStrategy2} that allows client-based recovery from Interest losses

\item \textbf{Revision 1 (July 1, 2014)}:
  Initial release

\end{itemize}


\clearpage
\tableofcontents
\clearpage


\input{intro}\clearpage
\input{face}\clearpage
\input{tab}\clearpage
\input{fw}\clearpage
\input{strategy}\clearpage
\input{mgmt}\clearpage
\input{rib}\clearpage
\input{cong}\clearpage
\input{security}\clearpage
\input{core}\clearpage
\input{testing}\clearpage

\phantomsection
\addcontentsline{toc}{section}{References}

\bibliographystyle{IEEEtran}
\bibliography{ref,rfc}

\end{document}
