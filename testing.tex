\section{Testing}\label{sec:test}

In general, software testing consists of multiple levels of testing.
The levels that have primarily been supported during the NFD development process include unit and integration tests.

At the \emph{unit test level}, individual units of source code (usually defined in a single \texttt{.cpp} file) are tested for functional correctness.
Some of the developed unit tests in NFD involve multiple modules and interaction between modules (e.g., testing of forwarding strategies).
However, even in these cases, all testing is performed internally to the module, without any external interaction.

NFD also employs \emph{integration testing}, where NFD software and its components is evaluated as a whole in a controlled networking environment.

\subsection{Unit Tests}\label{sec:test-unit}

NFD uses the Boost.Test library~\cite{boost-test} to support the development and execution of unit tests.

\subsubsection{Test Structure}\label{sec:test-unit-struct}

Each unit test consists of one or more test suites, which may contain one or more locally defined classes and/or attributes and a number of test cases.
A specific test case may test one or more functionalities implemented by a NFD module.
In order to reuse common functions among the test suites of a test file or even among multiple test files, one can make use of the fixture model provided by the Boost.Test library.

For example, the \path{tests/daemon/face/face.t.cpp} file, which tests the correctness of the \path{daemon/face/face.hpp} file, contains a single test suite with a number of test cases, and employs the test-only classes DummyFace and DummyTransport, described below.
These test cases check the correctness of the functionalities implemented by \path{daemon/face/face.hpp} and \path{daemon/face/face.cpp}.

\subsubsection{Running Tests}\label{sec:test-unit-run}

In order to run the unit tests, one has to configure and compile NFD with the \verb|--with-tests| command-line option (using \verb|--debug| is also strongly recommended while developing).
Once the compilation is done, one can run all the unit tests by typing:

\begin{verbatim}
    ./build/unit-tests-<module>
\end{verbatim}

\noindent
One can run a specific test case of a specific test suite by typing:

\begin{verbatim}
    ./build/unit-tests-<module> -t <TestSuite>/<TestCase>
\end{verbatim}

\subsubsection{Test Helpers}\label{sec:test-unit-helper}

The ndn-cxx library provides a number of helper tools to facilitate development of unit tests:

\begin{itemize}
\item \textbf{DummyClientFace} \texttt{<ndn-cxx/util/dummy-client-face.hpp>}:
  a socket-independent Face abstraction to be used during the unit testing process.
\item \textbf{UnitTestSystemClock} and \textbf{UnitTestSteadyClock} \texttt{<ndn-cxx/util/time-unit-test-clock.hpp>}:
  abstractions to mock the system and steady clocks used inside ndn-cxx and NFD implementation.
\end{itemize}

In addition to library tools, NFD's unit testing environment also includes a few NFD-specific common testing helpers and fixtures:

\begin{itemize}

\item \textbf{ClockFixture} (\texttt{tests/clock-fixture.hpp}):
  a base fixture that overrides steady clock and system clock.

\item \textbf{KeyChainFixture} (\texttt{tests/key-chain-fixture.hpp}):
  a base fixture that provides an in-memory ephemeral KeyChain and can be used to create temporary identities.
  Identities added via \texttt{KeyChainFixture::addIdentity} are automatically deleted during test case teardown.

\item \textbf{LimitedIo} (\texttt{tests/daemon/limited-io.hpp}):
  class to execute a limited number of I/O operations.
  It allows to specify either an upper bound to the number of operations or an execution time limit, which will fail the test case if not respected.
  Note that this class is not a fixture.

\item \textbf{DummyTransport} (\texttt{tests/daemon/face/dummy-transport.hpp}):
  a dummy \texttt{Transport} subclass used in testing Faces and LinkServices.

\item \textbf{DummyLinkService} (\texttt{tests/daemon/face/dummy-link-service.hpp}):
  a dummy \texttt{LinkService} subclass that logs all sent and received packets.

\item \textbf{StrategyTester} (\texttt{tests/daemon/fw/strategy-tester.hpp}):
  a framework to test a forwarding strategy.
  This helper extends the tested strategy to offer recording of its invoked actions, without passing them to the actual forwarder implementation.

\item \textbf{TopologyTester} (\texttt{tests/daemon/fw/topology-tester.hpp}):
  a framework to construct a virtual mock topology and implement various network events (e.g., failing and recovering links).
  The purpose is to test the behavior of the forwarder core plus a forwarding strategy in a simulated network topology.

\end{itemize}

\subsubsection{Test Code Guidelines and Naming Conventions}\label{sec:test-unit-guideline}

The current implementation of NFD unit tests uses the following naming convention:

\begin{itemize}

\item A test suite for the \path{folder/module-name.hpp} file (and its corresponding \texttt{.cpp} file) should be placed in \path{tests/folder/module-name.t.cpp}.
  For example, the test suite for \path{daemon/fw/forwarder.hpp} should be placed in \path{tests/daemon/fw/forwarder.t.cpp}.

\item A test suite should be named \texttt{TestModuleName} and nested under one or more test suites named after the enclosing directories.
  For example, the test suite for the \path{daemon/fw/forwarder.hpp} file should be named \texttt{Fw/TestForwarder} (the ``daemon'' part is not needed, as all daemon-related unit tests are split into a dedicated test binary \texttt{unit-tests-daemon}).

\item All test suites and test cases should be declared inside the \texttt{nfd::tests} namespace.
  If needed, the module under test (or the enclosing namespace) can be imported with the C++ \texttt{using} (or \texttt{using namespace}) directive.

\item A test suite or test case that needs access to the global \verb|io_context| instance or that interacts with it in any way should use \texttt{GlobalIoFixture} to get proper automatic setup and teardown of the \verb|io_context|.
  In particular, this guideline applies to most test suites under \path{tests/daemon/}.
  If a custom fixture is required for such a test suite or test case, the custom fixture should derive from \texttt{GlobalIoFixture}.
  If a test case needs to sign Data, it should use \texttt{KeyChainFixture} or a fixture that derives from the latter.
  When feasible, \texttt{ClockFixture} or \texttt{GlobalIoTimeFixture} should be used to mock clocks.

\end{itemize}

\subsection{Integration Tests}\label{sec:test-integ}

The NFD team has developed a number of integrated test cases (\url{https://gerrit.named-data.net/admin/repos/NFD/integration-tests}) that can be run in a dedicated test environment.

The easiest way to run the integration tests is to use Vagrant (\url{https://www.vagrantup.com/}) to automate creation and running of VirtualBox virtual machines.
Note that the test machine will need at least 9 GB of RAM and at least 5 CPU cores.

\begin{itemize}
\item Install VirtualBox and Vagrant on the test system.
\item Clone the integration tests repository:
\begin{verbatim}
    git clone https://gerrit.named-data.net/NFD/integration-tests
    cd integration-tests
\end{verbatim}
\item Run \texttt{run-vagrant-tests.sh} script that will create necessary virtual machines and run all the integration tests.
\item Resulting logs can be collected using the \texttt{collect-logs.sh} script.
\end{itemize}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
