\section{Common Services}
\label{sec:core}

NFD contains several common services to support forwarding and management operations.
These services are an essential part of the source code, but are logically separated and placed into the \texttt{core/} folder (if they are shared with other binaries such as the \texttt{nfdc} tool) or into the \texttt{daemon/common/} folder (if used exclusively by the NFD binary).

In addition to core services, NFD also relies extensively on the \textbf{ndn-cxx} support library, which provides many basic functions such as: packet format encoding/decoding, data structures for management protocol, and security framework.
The latter, within the context of NFD, is described in more detail in Section~\ref{sec:security}.

\subsection{Configuration File}\label{sec:core-config}

Many aspects of NFD are configurable through a configuration file, which adopts the Boost INFO format~\cite{boost-property-tree}.
This format is very flexible and allows any combination of nested configuration structures.

\subsubsection{User Info}

Currently, NFD defines 6 top level configuration sections: \texttt{general}, \texttt{log}, \texttt{tables}, \texttt{face\_system}, \texttt{authorizations}, and \texttt{rib}.

\begin{itemize}

\item \textbf{general}:
The general section defines various parameters affecting the overall behavior of NFD.
Currently, the implementation only allows \texttt{user} and \texttt{group} parameter settings.
These parameters define the effective user and effective group that NFD will run as.
Note that using an effective user and/or group is different from just dropping privileges.
Namely, it allows NFD to regain superuser privileges at any time.
By default, NFD must be initially run with and be allowed to regain superuser privileges in order to access raw ethernet interfaces (Ethernet face support) and create a socket file in the system folder (Unix face support).
Temporarily dropping privileges by setting the effective user and group id provides minimal security risk mitigation, but it can also prevent well intentioned, but buggy, code from harming the user's system.
It is also possible to run NFD without superuser privileges, but it requires the disabling of ethernet faces (or proper configuration to allow non-root users to perform privileged operations on sockets) and modification of the Unix socket path for NFD and all applications (see your installed \verb|nfd.conf| configuration file or \verb|nfd.conf.sample| for more details).
When applications are built using the ndn-cxx library, the Unix socket path for the application can be changed using the \verb|client.conf| file. The library will search for \verb|client.conf| in three specific locations and in the following order:
\begin{itemize}
\item \verb|~/.ndn/client.conf|
\item \verb|SYSCONFDIR/ndn/client.conf| (if compiling from source, SYSCONFDIR is \verb|/usr/local/etc| by default)
\item \verb|/etc/ndn/client.conf|
\end{itemize}

\item \textbf{log}:
The log section defines the logger configuration such as the default log level and individual NFD component log level overrides.  The log section is described in more detail in Section~\ref{sec:common-logging}.

\item \textbf{tables}:
The tables section configures NFD's tables: Content Store, PIT, FIB, Strategy Choice, Measurements, and Network Region.
Currently supported settings include the following:

\begin{itemize}
\item \verb|cs_max_packets|: Content Store capacity limit in number of packets. The default is 65536, which corresponds to about 500~MB, assuming maximum size of 8~KB per Data packet.
\item \verb|cs_policy|: The Content Store replacement policy. Can be set to \verb|lru| (the default) or \verb|priority_fifo|.
\item \verb|cs_unsolicited_policy|: The policy used by the Content Store to decide whether to admit or drop an incoming unsolicited Data packet. Available policies are: \verb|drop-all| (the default), \verb|admit-local|, \verb|admit-network|, \verb|admit-all|.
\item \verb|strategy_choice|: This subsection selects the initial forwarding strategy for each specified prefix. Entries are listed as \verb|<namespace> <strategy-name>| pairs (space-separated).
\item \verb|network_region|: This subsection contains a set of network regions used by the forwarder to determine if an Interest carrying a forwarding hint has reached the producer region. Entries are a list of names.
\end{itemize}

\item \textbf{face\_system}:
The face system section fully controls the allowed face protocols, channels and channel creation parameters, and settings of multicast faces.  Specific protocols may be disabled by commenting out or removing the corresponding nested block in its entirety. Present but empty sections will result in enabling the corresponding protocol with its default parameters.

NFD supports the following face protocols:

\begin{itemize}
\item \textbf{unix}: Unix protocol.

This section can contain the following parameter:
  \begin{itemize}
  \item \verb|path|: sets the path for the stream-oriented Unix socket (the default is \verb|/run/nfd/nfd.sock| on Linux and \verb|/var/run/nfd/nfd.sock| on all other platforms)
  \end{itemize}

Note that if the \textbf{unix} section is present, the created Unix channel will always be in a ``listening'' state. Commenting out the \textbf{unix} section disables Unix channel creation.

\item \textbf{tcp}: TCP protocol.

This section can contain the following parameters:
  \begin{itemize}
  \item \verb|listen|: controls whether the created TCP channel is in listening mode and creates TCP faces when an incoming connection is received (enabled by default)
  \item \verb|port|: sets the TCP listener port number (default is 6363)
  \item \verb|enable_v4|: controls whether IPv4 TCP channels are enabled (enabled by default)
  \item \verb|enable_v6|: controls whether IPv6 TCP channels are enabled (enabled by default)
  \end{itemize}

\item \textbf{udp}: UDP protocol.

This section can contain the following parameters:
  \begin{itemize}
  \item \verb|listen|: controls whether the created UDP channel is in listening mode and creates UDP unicast faces when incoming packets are received from a new peer (enabled by default)
  \item \verb|port|: sets UDP unicast port number (default is 6363)
  \item \verb|enable_v4|: controls whether IPv4 UDP channels are enabled (enabled by default)
  \item \verb|enable_v6|: controls whether IPv6 UDP channels are enabled (enabled by default)
  \item \verb|idle_timeout|: sets the time in seconds before closing an idle on-demand UDP unicast face (default is 600 seconds)
  \item \verb|mcast|: controls whether UDP multicast faces need to be created (enabled by default)
  \item \verb|mcast_group|: UDP IPv4 multicast group (default is 224.0.23.170)
  \item \verb|mcast_port|: sets UDP multicast port number for IPv4 (default is 56363)
  \item \verb|mcast_group_v6|: same as \verb|mcast_group| but for IPv6 faces
  \item \verb|mcast_port_v6|: same as \verb|mcast_port| but for IPv6 faces
  \item \verb|mcast_ad_hoc|: controls whether all UDP multicast faces are considered \emph{ad hoc} (disabled by default)
  \end{itemize}

\item \textbf{ether}: Ethernet protocol (NDN directly on top of Ethernet, without requiring IP).

This section can contain the following parameters:
  \begin{itemize}
  \item \verb|listen|: controls whether the created Ethernet channel is in listening mode and creates Ethernet unicast faces when incoming packets are received from a new peer (enabled by default)
  \item \verb|idle_timeout|: sets the time in seconds before closing an idle on-demand Ethernet unicast face (default is 600 seconds)
  \item \verb|mcast|: controls whether Ethernet multicast faces need to be created (enabled by default)
  \item \verb|mcast_group|: sets the Ethernet multicast group (default is \texttt{01:00:5E:00:17:AA})
  \item \verb|mcast_ad_hoc|: controls whether all Ethernet multicast faces are considered \emph{ad hoc} (disabled by default)
  \end{itemize}

\item \textbf{websocket}: WebSocket protocol (to connect to NFD from JavaScript applications running in a web browser).

This section can contain the following parameters:
  \begin{itemize}
  \item \verb|listen|: controls whether the created WebSocket channel is in listening mode and creates WebSocket faces when incoming connections are received (enabled by default)
  \item \verb|port|: WebSocket listener port number (default is 9696)
  \item \verb|enable_v4|: controls whether IPv4 WebSocket channels are enabled (enabled by default)
  \item \verb|enable_v6|: controls whether IPv6 WebSocket channels are enabled (enabled by default)
  \end{itemize}

\end{itemize}

\item \textbf{authorizations}:
The \verb|authorizations| section provides a fine-grained control for management operations.  As described in Section~\ref{sec:mgmt}, NFD has several managers, the use of which can be authorized to specific NDN users.
For example, the creation and destruction of faces can be authorized to one user, management of FIB to another, and control over strategy choice to a third user.

To simplify the initial bootstrapping of NFD, the sample configuration file does not restrict local NFD management operations: any user can send management commands to NFD and NFD will authorize them.  However, such configuration should not be used in a production environment and only designated users should be authorized to perform specific management operations.

The basic syntax for the \verb|authorizations| section is as follows.  It consists of zero or more \verb|authorize| blocks. Each \verb|authorize| block associates a single NDN identity certificate, specified by the \verb|certfile| parameter, with \verb|privileges| blocks. The \verb|privileges| block
defines a list of permissions/managers (one permission per line) that are granted to the user identified by
\verb|certfile| defines a file name (relative to the configuration file format) of the NDN certificate.
As a special case, primarily for demo purposes, certfile accepts value "any", which denotes any certificate possessed by any user.
Note that all managers controlled by the \verb|authorizations| section are local.  In other words, all commands start with \verb|/localhost|, which are possible only through local faces (Unix face and TCP face to 127.0.0.1).

\textbf{Note for developers:}

The \verb|privileges| block can be extended to support additional permissions with the creation of new managers (see Section~\ref{sec:mgmt}). This is achieved by deriving the new manager from the \verb|ManagerBase| class. The second argument to the \verb|ManagerBase| constructor specifies the desired permission name.

\item \textbf{rib}:
The \verb|rib| section controls behavior and security parameters for NFD RIB manager.
This section can contain three subsections: \verb|localhost_security|, \verb|localhop_security|, and \verb|auto_prefix_propagate|.
\verb|localhost_security| controls authorizations for registering and unregistering prefixes in RIB from local users (through local faces: Unix socket or TCP tunnel to 127.0.0.1).
\verb|localhop_security| defines authorization rules for so called localhop prefix registrations: registration of prefixes on the next hop routers.
\verb|auto_prefix_propagate| configures the behavior of the Auto Prefix Propagator feature of the RIB manager (Section~\ref{sec:rib-propagation}).

Unlike the main \verb|authorizations| section, the rib security section uses a more advanced validator configuration, thus allowing a greater level of flexibility in specifying authorizations.
In particular, it is possible to specify not only specific authorized certificates, but also indirectly authorized certificates.
For more details about validator configuration and its capabilities, refer to Section~\ref{sec:security} and \href{https://named-data.net/doc/ndn-cxx/current/tutorials/security-validator-config.html}{Validator Configuration File Format specification}~\cite{validatorConf}.

Similar to the \verb|authorizations| section, the sample configuration file, allows any local user to send register and unregister commands (\verb|localhost_security|) and prohibits remote users from sending registration commands (the \verb|localhop_security| section is disabled).
On NDN Testbed hubs, the latter is configured in a way to authorize any valid NDN Testbed user (i.e., a user possessing a valid NDN certificate obtained through NDNCERT~\cite{ndncert}) to send registration requests for the user's namespace.
For example, a user Alice with a valid certificate \texttt{/ndn\allowbreak /site\allowbreak /alice\allowbreak /KEY\allowbreak /...\allowbreak /ID-CERT/...} would be allowed to register any prefixes starting with \verb|/ndn/site/alice| on an NDN hub.

The \verb|auto_prefix_propagate| subsection supports configuring the forwarding cost of remotely registered prefixes, the timeout interval of a remote prefix registration command, how often propagations are refreshed, and the minimum and maximum wait time before retrying a propagation:
\begin{itemize}
\item \verb|cost|: The forwarding cost for prefixes registered on a remote router (default is 15).
\item \verb|timeout|: The timeout (in milliseconds) of prefix registration commands for propagation (default is 10000).
\item \verb|refresh_interval|: The interval (in seconds) before refreshing the propagation (default is 300). This setting should be less than \verb|face_system.udp.idle_time|, so that the face is kept alive on the remote router.
\item \verb|base_retry_wait|: The base wait time (in seconds) before retrying propagation (default is 50).
\item \verb|max_retry_wait|: maximum wait time (in seconds) before retrying propagation between consecutive retries (default is 3600). The wait time before each retry is calculated based on the following back-off policy: initially, the wait time is set to \verb|base_retry_wait|. The wait time is then doubled for each retry unless it is greater than \verb|max_retry_wait|, in which case the wait time is set to \verb|max_retry_wait|.
\end{itemize}

\end{itemize}

\subsubsection{Developer Info}

When creating a new management module, it is very easy to make use of the NFD configuration file framework.
Most heavy lifting is performed using the Boost.PropertyTree~\cite{boost-property-tree} library and NFD implements an additional wrapper (\texttt{ConfigFile}) to simplify configuration file operations.

\begin{enumerate}

\item Define the format of the new configuration section.
Reusing an existing configuration section could be problematic, since a diagnostic error will be generated any time an unknown parameter is encountered.

\item The new module should define a callback with prototype \texttt{void(*)(ConfigSection..., bool isDryRun)} that implements the actual processing of the newly defined section.
The best guidance for this step is to take a look at the existing source code of one of the managers and implement the processing in a similar manner.
The callback can support two modes: dry-run to check validity of the specified parameters, and actual run to apply the specified parameters.

As a general guideline, the callback should be able to process the same section multiple times in actual run mode without causing problems.
This feature is necessary in order to provide functionality of reloading configuration file during run-time.  In some cases, this requirement may result in cleaning up data structures created during the run.
If it is hard or impossible to support configuration file reloading, the callback must detect the reloading event and stop processing it.

\item Update NFD initialization in \verb|daemon/nfd.hpp| and \verb|daemon/nfd.cpp| files.  In particular, an instance of the new management module needs to be created inside the \texttt{initializeManagement} method.  Once module is created, it should be added to ConfigFile class dispatch.  Similar updates should be made to \verb|reloadConfigFile| method.

As another general recommendation, do not forget to create proper test cases to check correctness of the new config section processing.  This is vital for providing longevity support for the implemented module, as it ensures that parsing follows the specification, even after NFD or the supporting libraries are changed.

\end{enumerate}

\subsubsection{Configuration Reload}\label{sec:core-config-reload}

NFD reloads the configuration upon receiving a \textbf{SIGHUP} signal.

\subsection{Logging}\label{sec:common-logging}

One of the most important core services is the logging subsystem.
NFD's logger provides support for multiple log levels, which can be configured in the configuration file individually for each module.
The configuration file also includes a setting for the default log level that applies to all modules, except those explicitly listed.

\subsubsection{User Info}

Log level is configured in the \verb|log| section of the configuration file.
The format for each configuration setting is a key-value pair, where key is name of the specific module and value is the desired log level.
Valid values for log level are:

\begin{itemize}
\item \textbf{NONE}: no messages
\item \textbf{ERROR}: show only error messages
\item \textbf{WARN}: show also warning messages
\item \textbf{INFO}: show also informational messages (default)
\item \textbf{DEBUG}: show also debugging messages
\item \textbf{TRACE}: show also trace messages
\item \textbf{ALL}: all messages for all log levels (most verbose)
\end{itemize}

Individual module names can be found in the source code by looking for \verb|NFD_LOG_INIT(<module name>)| statements in \verb|.cpp| files, or using the \verb|--modules| command-line option of the \verb|nfd| program.
There is also a special \verb|default_level| key, which defines the log level for all modules that are not explicitly listed in the configuration (if not specified, the default log level is \verb|INFO|).

Log levels can also be configured via the \verb|NDN_LOG| environment variable. See \verb|ndn-log(7)| manual page (part of ndn-cxx) for syntax and examples.

\subsubsection{Developer Info}

To enable logging in a new module, very few actions are required from the developer:

\begin{itemize}
\item include \verb|"common/logger.hpp"| header file
\item declare the new logging module in a \texttt{.cpp} file using the \verb|NFD_LOG_INIT(<module name>)| macro
\item use \verb|NFD_LOG_<LEVEL>("statement to log")| macros in the source code
\end{itemize}

\subsection{Global Scheduler}\label{sec:common-scheduler}

The ndn-cxx library includes a scheduler class that provides a simple way to schedule arbitrary events (callbacks) at arbitrary time points.  Normally, each module/class creates its own scheduler object.
An implication of this is that a scheduled object, when necessary, must be cancelled in a specific scheduler, otherwise the behavior is undefined.

NFD packet forwarding has a number of cases that require shared ownership of scheduled events.
To simplify this and other event operations, NFD includes a per-thread global scheduler.
To use this scheduler, one needs to include \verb|daemon/common/global.hpp| and call \verb|getScheduler()| to obtain a reference to the global scheduler for the current thread.

\subsection{Global I/O Service}

The NFD packet forwarding implementation is based on Boost.Asio~\cite{boost-asio}, which provides efficient asynchronous operations.
The main feature of this is the \verb|io_context| abstraction.
\verb|io_context| implements the dispatching of any scheduled events in an asynchronous manner, such as sending packets through operating system sockets, processing received packets and incoming connections, and many others including arbitrary function calls (e.g., the \texttt{Scheduler} class in ndn-cxx library is also based on this abstraction).

Logically, \verb|io_context| is just a queue of callbacks (explicitly or implicitly added).
In order to actually execute any of these callback functions, at least one application thread must call the \verb|io_context::run| method.
The thread that called the \verb|run| method then starts processing enqueued callbacks in an application-defined manner.

The current version of NFD uses a single global instance of \verb|io_context| in each execution thread.
Specifically, both the main and the RIB thread call the \verb|run| method on their own instance of \verb|io_context|.
This logic is implemented in \verb|daemon/main.cpp|.

Some NFD classes may require to specify an \verb|io_context| object.
For example, to create a TCP face, it is necessary to provide an \verb|io_context| reference as a constructor parameter to \texttt{boost::\allowbreak asio::\allowbreak ip::\allowbreak tcp::\allowbreak socket}.
In such cases, it is sufficient to include the header file \verb|daemon/common/global.hpp| and supply \verb|getGlobalIoService()| as the argument.
The rest will be handled by the existing NFD framework.

\subsection{Hash Computation Routines}

Common services also include several hash functions, based on city hash algorithm~\cite{cityhash}, to support fast name-based operations.
Since efficient hash table index size depends on the platform, NFD includes several versions, for 16-bit, 32-bit, 64-bit, and 128-bit hashing.

The NameTree implementation generalizes the platform-dependent use of hash functions using a template-based helper (see \verb|computeHash| function in \texttt{daemon/table/name-tree-hashtable.cpp}).
Depending on the size of \verb|size_t| type on the platform, the compiler will automatically select the correct version of the hash function.

Other hash functions may be included in the future to provide tailored implementations for specific usage patterns.
In other words, since the quality of the hash function is usually not the sole property of the algorithm, but also relies on the hashed source (hash functions need to hash uniformly into the hash space), depending on which Interest and Data names are used, other hash functions may be more appropriate.
Cryptographic hash functions are also an option, however they are usually prohibitively expensive.

\subsection{Privilege Helper}

When NFD is run as a super user (may be necessary to support Ethernet faces, enabling TCP/UDP/WebSocket faces on privileged ports, or enabling Unix socket face in a root-only-writeable location), it is possible to run most of the operations in unprivileged mode.
In order to do so, NFD includes a \texttt{PrivilegeHelper} class that can be configured through configuration file to drop privileges as soon as NFD initialization finishes.
When necessary, NFD can temporarily regain privileges to do additional tasks, e.g., (re-)create multicast faces.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
