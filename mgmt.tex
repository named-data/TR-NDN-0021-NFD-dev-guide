\section{Management}\label{sec:mgmt}

NFD management offers the capability to monitor and control NFD through a configuration file and an Interest-based API.

NFD management is divided into several management modules.
Each management module is responsible for a subsystem of NFD.
It can initialize the subsystem from a section in the NFD configuration file, or offer an Interest-based API to allow others to monitor and control the subsystem.

Section~\ref{sec:mgmt-overview} gives an overview of the NFD configuration file and of the basic mechanisms in NFD Management protocol~\cite{nfd-management}.
Section~\ref{sec:mgmt-dispatch} describes the dispatcher and authenticator used in the protocol implementation.
Section~\ref{sec:mgmt-status}, \ref{sec:mgmt-face}, \ref{sec:mgmt-fib}, \ref{sec:mgmt-sc} explain the details of the four major management modules.
Section~\ref{sec:mgmt-conf} introduces two additional management modules that are used in configuration file parsing.
Section~\ref{sec:mgmt-extend} offers ideas on how to extend NFD management.


\subsection{Protocol Overview}\label{sec:mgmt-overview}

All management actions that change NFD state require the use of \emph{control commands}~\cite{control-command}, a form of signed Interests.
These allow NFD to determine whether the issuer is authorized to perform the specified action.
Management modules respond with \emph{control responses} to inform the user of the command success or failure.
Control responses have status codes similar to HTTP, and describe the action that was performed or any errors that occurred.

Management actions that just query the current state of NFD do not need to be authenticated.
These actions are defined in NFD Management Protocol~\cite{nfd-management} as \emph{status datasets}, and are currently implemented in NFD as a simple Interest/Data exchange.


\subsection{Dispatcher and Authenticator}\label{sec:mgmt-dispatch}

As shown in figure~\ref{fig:mgmt:manager:dispatch}, managers utilize \texttt{ndn::Dispatcher} as an abstraction to deal with common Interest/Data processing, so that they can focus on performing the required low-level operations on the module of NFD that they manage.

\begin{figure}[htpb]
\centering
\includegraphics[width=.8\textwidth]{mgmt-manager-dispatch}
\caption{Overview of the manager Interest/Data processing via the dispatcher.}
\label{fig:mgmt:manager:dispatch}
\end{figure}

More specifically, a manager always consists of a series of handlers, each of which is responsible for dealing with a \emph{control command} request or a \emph{status dataset} request.
Per the management protocol, these requests follow a namespace pattern of \path{/localhost/nfd/<manager-name>/<verb>}.
Here, \emph{verb} describes the action that the \emph{manager-name} manager should perform.
For example, \path{/localhost/nfd/fib/add-nexthop} directs the FIB Manager to add a next hop (command arguments follow the verb).

A manager registers a handler with the dispatcher with a partial name that is composed of the manager's name and a verb.
After all managers have registered their handlers, the dispatcher creates full prefixes using those partial names plus each registered top-level prefix (e.g., \texttt{/localhost\allowbreak /nfd}), and then sets up interest filters for them.
When a request arrives to the dispatcher, it is directed to some management handler after being processed by interest filters.
On the other hand, all types of responses, such as of \emph{control responses}, \emph{status datasets}, etc., go through the dispatcher, for concatenation, segmentation, and signing.

For \emph{control commands}, a handler is always registered along with an \texttt{authorization} method and a \texttt{parametersValidate} method.
A request can be directed to the handler if and only if it is accepted by \texttt{authorization} and its \emph{control parameters} can be validated by \texttt{parametersValidate}.
For \emph{status datasets}, there is no need to validate any parameters, therefore the \texttt{authorization} is made to accept all requests\footnote{This may be changed whenever data access control is desired.}.

Furthermore, managers that produce notifications, such as the FaceManager, will also register \emph{notification streams} to the dispatcher.
This type of registration returns a \texttt{postNotification}, through which the manager will only need to generate the notification content, leaving the rest of the work (constructing the packet, signing, etc.) to the dispatcher.

\subsubsection{Manager Base}\label{sec:mgmt-base}

\texttt{ManagerBase} is the base class for all managers.
This class holds the manager's shared \texttt{Dispatcher} and \texttt{CommandValidator}, and provides a number of commonly used methods.
In particular, \texttt{ManagerBase} provides the methods to register a command handler, to register a status dataset handler, and to register notification streams.
Moreover, \texttt{ManagerBase} also provides convenience methods for authorizing \emph{control commands} (\texttt{authorize}), for validating \emph{control parameters} (\texttt{validateParameters}), and also for extracting the name of the requester from the Interest (\texttt{extractRequester}).

On construction, \texttt{ManagerBase} obtains a reference to a \texttt{Dispatcher}, that is responsible to dispatch requests to the target management handler, and a \texttt{CommandValidator}, that will be used for control command authorization later.
Derived manager classes provide the \texttt{ManagerBase} constructor with the name of their privilege (e.g., \texttt{faces}, \texttt{fib}, \texttt{strategy-choice}).
This privilege is used when specifying the set of authorized capabilities for a given NDN identity certificate in the configuration file.

\subsubsection{Command Authenticator}\label{sec:mgmt-authenticator}

The \texttt{CommandAuthenticator} provides authentication and authorization of control commands.

Rules for authentication and authorization are specified in the ``\texttt{authorizations}'' section of the NFD configuration file.
In that section, each \texttt{authorize} subsection lists a single NDN certificate and a set of privileges.
An example of an \texttt{authorize} subsection is:

\begin{verbatim}
authorize
{
  certfile keys/operator.ndncert
  privileges
  {
    faces
    strategy-choice
  }
}
\end{verbatim}

Unlike most other validators, \texttt{CommandAuthenticator} does not attempt to retrieve signing certificates during the authentication step.
This is because the authenticator needs to operate before routes are established.
Thus, every certificate used to sign control commands must be explicitly specified in the configuration.
The value in the ``\texttt{certfile}'' key is a filename that refers to an NDN certificate (version 1); it can be either an absolute path, or a path relative to the configuration file.
For convenience in non-production systems, a special value ``\texttt{any}'' can be used in ``\texttt{certfile}'', granting any signing certificate the privileges listed in the ``\texttt{privileges}'' subsection.
Under the ``\texttt{privileges}'' subsection, a set of privileges can be specified, where each privilege is the management module name.
Supported privileges are: \texttt{faces}, \texttt{fib}, \texttt{strategy-choice}.

The NFD initialization procedure creates a \texttt{CommandAuthenticator} instance and invokes \texttt{setConfigFile}, in which the authenticator registers itself as a processor of the ``\texttt{authorizations}'' section of NFD configuration file.
The authenticator instance is then given to the constructor of each manager that accepts control commands.
The manager invokes \texttt{CommandAuthenticator::\allowbreak makeAuthorization} method for each accepted command verb, which returns an \texttt{ndn::\allowbreak mgmt::\allowbreak Authorization} callback function that can be used with the dispatcher; this \texttt{makeAuthorization} invocation also tells the authenticator which management module names are supported.
Finally, the initial configuration is parsed, which populates the internal data structures of the authenticator.

When a control command arrives, the dispatcher passes the command Interest to the \texttt{ndn::mgmt::Authorization} callback function returned by \texttt{CommandAuthenticator::\allowbreak makeAuthorization}.
This authorization callback has four steps:

\begin{enumerate}
\item Based on management module and command verb, find out what signing certificates are allowed.
      Although the dispatcher does not directly pass management module and command verb to the authorization callback, this information is captured when the authorization callback was initially constructed, because the manager invokes \texttt{CommandAuthenticator::\allowbreak makeAuthorization} once for every distinct command verb and thus each gets a different authorization callback instance.
\item If ``\texttt{certfile any}'' applies to this management module and command verb, the control command is authorized, and the remaining steps are skipped.
\item Check whether the Interest is signed by one of the listed certificates by inspecting the KeyLocator field.
      If not, the control command is rejected.
\item Pass the Interest to the \texttt{ndn::security::CommandInterestValidator} which checks the timestamps on command Interests to prevent replay attacks.
      A single command Interest validator instance is shared among all authorization callbacks.
      If the timestamp is unacceptable, the control command is rejected.
\item Finally, the signature is verified against the public key using \texttt{Validator::verifySignature} function, and the control command is either accepted or rejected based on the result of the signature verification.
\end{enumerate}

The benefit of this design is that it takes advantage of the management module and command verb information that is already known from the dispatcher, so that name lookups are not repeated to determine permissible signing certificates.

A weakness in this procedure is the risk of denial-of-service attack: signature verification is performed after timestamp checking, therefore an attacker can forge command Interests with the KeyLocator of an authorized signing certificate in order to manipulate the internal state of the command Interest validator, and cause subsequent Interests signed by that certificate to be rejected\footnote{See \url{https://redmine.named-data.net/issues/3821}}.


\subsection{Forwarder Status}\label{sec:mgmt-status}

The Forwarder Status Manager (\texttt{nfd::ForwarderStatusManager}) provides information about NFD and basic statistics about the forwarder through the method \texttt{listGeneralStatus}, which is registered to the dispatcher with the name \path{status/general}.
The supplied information include NFD's version, startup time, Interest/Data/Nack packet counts, and various table entry counts, and is published with a freshness period of 1~second to avoid excessive processing overhead.


\subsection{Face Management}\label{sec:mgmt-face}

The Face Manager (\texttt{nfd::FaceManager}) creates and destroys faces for all configured protocol types.
Some attributes of an existing face can also be changed via this manager.
Currently, these attributes are the face persistency (Section~\ref{sec:face-face}) and, on local faces, whether local fields are allowed or not (Section~\ref{sec:face-ls-generic}).

\subsubsection{Command Processing}

On creation, the Face Manager registers three command handlers: \texttt{createFace}, \texttt{updateFace}, and \texttt{destroyFace}, with names \path{faces/create}, \path{faces/update} and \path{faces/destroy} respectively.

\begin{description}
\item[\texttt{createFace}] creates a unicast face, for protocols that support it.\\
\texttt{FaceUri} is used to parse the incoming URI in order to determine the type of face to create.
The URI must be canonical.
A canonical URI for UDP and TCP tunnels should specify either IPv4 or IPv6, have IP address instead of hostname, and contain port number (e.g., ``\texttt{udp4://192.0.2.1:6363}'' is canonical, but ``\texttt{udp://192.0.2.1}'' and ``\texttt{udp://example.net:6363}'' are not).
Non-canonical URI results in a code 400 ``Non-canonical URI'' control response.
The URI scheme (e.g., ``\texttt{tcp4}'', ``\texttt{tcp6}'', etc.) is used to lookup the appropriate \texttt{ProtocolFactory} via \texttt{FaceSystem::\allowbreak getFactoryByScheme}.
Failure to find a factory results in a code 406 ``Unsupported protocol'' control response.
Otherwise, the Face Manager calls \texttt{ProtocolFactory::\allowbreak createFace} method to initiate the potentially asynchronous process of face creation (DNS resolution, connection to remote host, etc.), supplying \texttt{afterCreateFaceSuccess} and \texttt{afterCreateFaceFailure} callbacks.
These callbacks will be called by the face system after the face is either successfully created or failed to be created, respectively.
Unauthorized, improperly-formatted, conflicting, or otherwise failing requests will be responded with appropriate failure codes and failure reasons.
Refer to the NFD Management protocol specification~\cite{nfd-management} for the list of possible error codes.
After a new Face has been created, the Face Manager adds the new Face to the Face Table\footnote{The Face Table is a table of Faces that is managed by the Forwarder. Using this table, the Forwarder assigns each Face a unique ID, manages active Faces, and performs lookups for a Face object by FaceId when requested by other modules.} and responds with a code 200 ``OK'' control response to the original control command.

\item[\texttt{updateFace}] can change the non-readonly properties of a face.\\
The target face can be determined by the FaceId extracted from the ControlParameters supplied to this command.
The FaceId could be set to zero, implying an update of the same face on which this command was received.
If the target face does not exist or the \texttt{IncomingFaceIdTag} is missing on the command Interest, this attempt of update fails with code 404.
All changes on the target face will be performed after validation, leading to a code 200 ``OK'' response whose body contains ControlParameters describing the updated properties.
On the contrary, if any of the requested changes is invalid, such as enabling local fields on a non-local face or changing the persistency to an unsupported value, the command will fail with a code 409 response whose body contains the invalid fields as ControlParameters.

\item[\texttt{destroyFace}] attempts to close the specified face.\\
The Face Manager responds with code 200 ``OK'' if the face is successfully destroyed or it cannot be found in the Face Table, but no errors occurred.
Note that the Face Manager does not directly remove the face from the Face Table, its removal is a side effect of calling \texttt{Face::close}.
\end{description}

\subsubsection{Datasets and Event Notification}

The Face Manager provides two datasets: Channel Status and Face Status.
The Channel Status dataset lists all channels (in the form of their local URI) that this NFD has created, and can be accessed under the \path{/localhost/nfd/faces/channels} namespace.
Face Status, similarly, lists all created Faces, but provides much more detailed information, such as flags and incoming/outgoing Interest/Data/Nack counts.
The Face Status dataset can be retrieved from the \path{/localhost/nfd/faces/list} namespace.

These datasets are returned when \texttt{listFaces} and \texttt{listChannels} methods are invoked.
A third method, \texttt{queryFaces}, serves to obtain the status of only those faces that satisfy a set of constraints, provided as a parameter.
When the Face Manger is constructed, it registers these three handlers to the dispatcher with the names \path{faces/list}, \path{faces/channels} and \path{faces/query} respectively.

In addition to these datasets, the Face Manager also publishes notifications when Faces are created, destroyed, go up, or go down.
This is done using the \texttt{postNotification} function returned after registering a notification stream to the dispatcher with the name \path{faces/events}.
Two methods, \texttt{afterFaceAdded} and \texttt{afterFaceRemoved}, that take the function \texttt{postNotification} as argument, are connected to the FaceTable's \texttt{onAdd} and \texttt{onRemove} signals~\cite{ndn-cxx-guide}.
Whenever these two signals are emitted, the connected methods will be invoked immediately, where the \texttt{postNotification} will be used to publish notifications through the dispatcher.


\subsection{FIB Management}\label{sec:mgmt-fib}

The FIB Manager (\texttt{nfd::FibManager}) allows authorized users (normally it is only the RIB Management, see Section~\ref{sec:rib}) to modify NFD's FIB.
It also publishes a dataset of all FIB entries and their next hops.

At a high-level, authorized users can request the FIB Manager to:
\begin{enumerate*}[(i)]
\item add a next hop to a prefix;
\item update the routing cost of a next hop;
\item remove a next hop from a prefix.
\end{enumerate*}
The first two capabilities correspond to the \texttt{add-nexthop} verb, while removing a next hop falls under \texttt{remove-nexthop}.
These two verbs are used along with the manager name \texttt{fib} to register the following handlers of control commands:

\begin{itemize}[nosep]
\item \texttt{addNextHop}: add next hop or update cost of existing next hop
\item \texttt{removeNextHop}: remove specified next hop
\end{itemize}
Note that \texttt{addNextHop} will create a new FIB entry if the requested entry does not already exist.
Similarly, \texttt{removeNextHop} will remove the FIB entry after removing the last next hop.

\paragraph{\textbf{FIB Dataset}}\
One status dataset handler, \texttt{listEntries}, is registered to the dispatcher with the name \path{fib/list}, to publish FIB entries according to the FIB dataset specification.
On invocation, the whole FIB is serialized in the form of a collection of \texttt{FibEntry} and nested \texttt{NextHopList} TLVs, which are appended to a \emph{StatusDatasetContext} of the dispatcher.


\subsection{Strategy Choice Management}\label{sec:mgmt-sc}

The Strategy Choice Manager (\texttt{nfd::StrategyChoiceManager}) is responsible for setting and unsetting forwarding strategies for the namespaces via the Strategy Choice table.
Note that setting/unsetting the strategy applies only to the local NFD.
Also, the current implementation requires that the selected strategy has been added to a pool of known strategies in NFD at compile time (see Section~\ref{sec:strategy}).
Attempting to change to an unknown strategy will result in a code 404 ``strategy not found'' response.
By default, there is at least the root prefix (``/'') available for strategy changes, which defaults to the ``best route'' strategy.
However, it is an error to attempt to unset the strategy for root (code 403).

Similar to the FIB and Face Managers, the Strategy Choice Manager registers, when constructed, two command handlers, \texttt{setStrategy} and \texttt{unsetStrategy}, as well as a status dataset handler \texttt{listChoices} to the dispatcher, with names \path{strategy-choice/set}, \path{strategy-choice/unset}, and \path{strategy-choice/list} respectively.

On invocation, \texttt{setStrategy} and \texttt{unsetStrategy} will set/unset the specified strategy, while \texttt{listChoices} will serialize the Strategy Choice table into \texttt{StrategyChoice} TLVs, and publish them as the dataset.


\subsection{Configuration Handlers}\label{sec:mgmt-conf}

\subsubsection{General Configuration File Section Parser}\label{sec:mgmt-conf-general}

The \texttt{general} namespace provides parsing for the identically named \texttt{general} configuration file section.
The NFD startup process invokes \texttt{setConfigSection} to trigger the corresponding \texttt{onConfig} method for the actual parsing.

At present, this section is limited to specifying an optional user and group name to drop the effective \texttt{uid} and \texttt{gid} of the running NFD process, if supported by the operating system, for safer operations.
The \texttt{general} section parser initializes a global \texttt{PrivilegeHelper} instance to perform the actual (de-)escalation work.

\subsubsection{Tables Configuration File Section Parser}\label{sec:mgmt-conf-tables}

\texttt{TablesConfigSection} provides parsing for the \texttt{tables} configuration file section.
This class can then configure the various NFD tables (Section~\ref{sec:tables}) appropriately.
Currently, the \texttt{tables} section supports changing Content Store capacity and cache replacement policy, per-prefix strategy choices, and network region names.
Like other configuration file parsers, \texttt{TablesConfigSection} is registered as the processor of its corresponding section by the NFD startup process via \texttt{setConfigFile} method, which invokes \texttt{onConfig}.


\subsection{How to Extend NFD Management}\label{sec:mgmt-extend}

Each manager is an interface for some part of the lower layers of NFD.
For example, the Face Manager handles Face creation/destruction.
The current set of managers are independent and do not interact with one another.
Consequently, adding a new manager is a fairly straightforward task; one only needs to determine what part(s) of NFD should be exported through an Interest/Data API and create an appropriate command Interest interpreter.

In general, NFD managers do not need to offer much functionality through a programmatic API.
Most managers only need to register request handlers or notification streams to \texttt{ndn::Dispatcher}, so that the corresponding requests are routed to them and the produced notifications can be sent out.
Some managers may also require to hook into the configuration file parser.
All managerial tasks to control NFD internals should be performed via the defined Interest/Data management protocol.
