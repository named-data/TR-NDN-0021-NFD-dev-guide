#!/usr/bin/env bash

# mkdir old-revs
# for i in $(seq 9); do
#     # git clone . old-revs/$i
#     # (cd old-revs/$i; git checkout revision-$i; make)
#     cp old-revs/$i/*.pdf old-revs/ndn-0021-$i-nfd-guide.pdf
# done

# input=$1
# if [[ ! -f $input ]]; then
#     echo "Usage:"
#     echo "    $0 <outdated-pdf>"
# fi

for i in $(seq 9); do
    pdflatex -jobname="old-revs/ndn-0021-$i-nfd-guide-outdated" "\def\origfile{old-revs/ndn-0021-$i-nfd-guide.pdf}\input{outdated}"
    pdflatex -jobname="old-revs/ndn-0021-$i-nfd-guide-outdated" "\def\origfile{old-revs/ndn-0021-$i-nfd-guide.pdf}\input{outdated}"
done

rm -f old-revs/*log old-revs/*aux old-revs/*out
