\section{Forwarding Strategy}\label{sec:strategy}

In NFD forwarding, forwarding strategies provide the intelligence to make decision on whether, when, and where to forwarding Interests.
Forwarding strategies, together with forwarding pipelines (Section~\ref{sec:fw}), make up the packet processing logic in NFD.
The forwarding strategy is triggered from the forwarding pipelines when a decision about Interest forwarding needs to be made.
In addition, the strategy can receive notifications on the outcome of its forwarding decisions, e.g., when a forwarded Interest is satisfied, timed out, or brings back a Nack.

Our experience with NDN applications has shown that different applications need different forwarding behaviors.
For example, a file retrieval application wants to retrieve contents from a content source with highest bandwidth, an audio chat application wants the lowest delay, and a dataset synchronization library (such as ChronoSync) wants to multicast Interests to all available faces in order to reach its peers.
The need for different forwarding behaviors motivates us to have multiple forwarding strategies in NFD.

Despite having multiple strategies in NFD, the forwarding decision of an individual Interest must be made by a single strategy.
NFD implements per-namespace strategy choice.
An operator may configure a specific strategy for a name prefix, and Interests under this name prefix will be handled by this strategy.
This configuration is recorded in the Strategy Choice table (Section~\ref{sec:tab-sc}), and is consulted by the forwarding pipelines.

Currently, the choice of forwarding strategy is a local configuration.
The same Interest may be handled by completely different strategies on different nodes.
Alternatives to this approach are being discussed as of Jan 2016.
One notable idea is \textit{routing annotations} where the routing announcement carries an indication about the preferred strategy.


\subsection{Strategy API}\label{sec:strategy-api}

Conceptually, a strategy is a program written for an abstract machine, the strategy API.
The ``language'' of this abstract machine contains standard arithmetic and logical operations, as well as interactions with the rest of NFD.
The state of this abstract machine is stored in NFD tables.

Each NFD strategy is implemented as a subclass of \texttt{nfd::fw::Strategy} class, which provides the API for the interaction between the implemented strategy and the rest of NFD.
This API is the only way a strategy can access NFD components, therefore available functionality in the strategy API determines what NFD strategies can or cannot do.

A strategy is invoked through one of the \textit{triggers} (Section~\ref{sec:strategy-trigger}).
The forwarding decision is made with \textit{actions} (Section~\ref{sec:strategy-action}).
Strategies are also allowed to store information on certain table entries (Section~\ref{sec:strategy-storage}).


\subsubsection{Triggers}\label{sec:strategy-trigger}

Triggers are entrypoints to the strategy program.
A trigger is declared as a virtual method of \texttt{nfd::fw::Strategy} class, and is expected to be overridden by a subclass.

\paragraph{After Receive Interest Trigger}\label{sec:strategy-after-receive-interest}\

This trigger is declared as \texttt{Strategy::afterReceiveInterest} method.
This method is pure virtual, and therefore must be overridden by a subclass.

When an Interest is received, passes necessary checks, and needs to be forwarded, \textit{Incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}) invokes this trigger with the Interest packet, its incoming face, and the PIT entry.
At that time, the following conditions hold for the Interest:
\begin{itemize}[nosep]
\item The Interest has not exceeded its \texttt{HopLimit}.
\item The Interest does not violate \texttt{/localhost} scope.
\item The Interest is not looped.
\item The Interest cannot be satisfied by the Content Store.
\item The Interest is under a namespace managed by this strategy.
\end{itemize}

After being triggered, the strategy should decide whether and where to forward this Interest.
Most strategies need a FIB entry to make this decision, which can be obtained by calling \texttt{Strategy::lookupFib} accessor function.
If the strategy decides to forward this Interest, it should invoke \textit{send Interest} action at least once; it can do so either immediately or some time in the future using a timer.%
\footnote{\textbf{Warning}: although a strategy is allowed to invoke \textit{send Interest} action via a timer, this forwarding may never happen in special cases.
For example, if while such a timer is pending an NFD operator updates the strategy on Interest's namespace, the timer event will be cancelled and new strategy may not decide to forward the Interest until after all out-records in the PIT entry expire.}
If the strategy concludes that this Interest cannot be forwarded, it should invoke the \texttt{Strategy::setExpiryTimer} action and set the timer to expire immediately, so that the PIT entry can eventually be deleted.

\paragraph{After Content Store Hit Trigger}\label{sec:strategy-after-cs-hit}\

This trigger is declared as \texttt{Strategy::afterContentStoreHit} method.
The base class provides a default implementation that sends the matching Data to the downstream.

\paragraph{After Receive Data Trigger}\label{sec:strategy-after-receive-data}\

This trigger is declared as \texttt{Strategy::afterReceiveData} method.
This trigger is invoked when an incoming Data satisfies exactly one PIT entry, and gives the strategy full control over Data forwarding.
When this trigger is invoked, the Data has been verified to satisfy the PIT entry, and the PIT entry expiry timer is set to fire immediately.
The base class implementation invokes the \texttt{Strategy::beforeSatisfyInterest} trigger and then returns the Data to all downstream faces.

Inside this trigger:
\begin{itemize}[nosep]
\item A strategy should return Data to downstream nodes via the methods \texttt{Strategy::sendData} or \texttt{Strategy::sendDataToAll}.
\item A strategy can modify the Data as long as it still satisfies the PIT entry, such as adding or removing congestion marks.
\item A strategy can delay Data forwarding by prolonging the PIT entry lifetime via \texttt{Strategy::setExpiryTimer}, and forward Data before the PIT entry is erased.
\item A strategy can collect measurements about the upstream.
\item A strategy can collect responses from additional upstream nodes by prolonging the PIT entry lifetime every time a Data is received.
Note that only one Data should be returned to each downstream node.
\end{itemize}

\paragraph{Before Satisfy Interest Trigger}\label{sec:strategy-before-satisfy-interest}\

This trigger is declared as \texttt{Strategy::beforeSatisfyInterest} method.
The base class provides a default implementation that does nothing; a subclass can override this method if the strategy needs to be invoked for this trigger, e.g., to record data plane measurement results for the pending Interest.

When a PIT entry is satisfied, before Data is sent to downstreams (if any), \textit{Incoming Data pipeline} (Section~\ref{sec:fw-incoming-data}) invokes this trigger with the PIT entry, the Data packet, and its incoming face.
The PIT entry may represent either a pending Interest or a recently satisfied Interest.

\paragraph{After Receive Nack Trigger}\label{sec:strategy-after-receive-nack}\

This trigger is declared as \texttt{Strategy::afterReceiveNack} method.
The base class provides a default implementation that does nothing, which means all incoming Nacks will be dropped, and will not be passed to downstreams.
A subclass can override this method if the strategy needs to be invoked for this trigger.

When an Interest is received, and passes necessary checks, \textit{Incoming Nack pipeline} (Section~\ref{sec:fw-incoming-nack}) invokes this trigger with the Nack packet, its incoming face, and the PIT entry.
At that time, the following conditions hold:
\begin{itemize}[nosep]
\item The Nack is received in response an forwarded Interest.
\item The Nack has been confirmed to be a response to the last Interest forwarded to that upstream, i.e. the PIT out-record exists and has a matching Nonce.
\item The PIT entry is under a namespace managed by this strategy\footnote{%
    This does not necessarily imply that the Interest was forwarded by this strategy.
    In case the effective strategy is changed after an Interest is forwarded and then a Nack comes back, the new effective strategy will be triggered.}.
\item The NackHeader has been recorded in the \textit{Nacked} field of the PIT out-record.
\end{itemize}

\noindent
After being triggered, the strategy could typically do one of the following:
\begin{itemize}[nosep]
\item Retry the Interest by forwarding it to the same or different upstream(s), by invoking \textit{send Interest} action.
      Most strategies need a FIB entry to find out potential upstreams, which can be obtained by calling \texttt{Strategy::lookupFib} accessor function.
\item Give up and return the Nack to downstream(s), by invoking \textit{send Nack} action.
\item Do nothing for this Nack.
      If some but not all upstreams have Nacked, the strategy may want to wait for Data or Nack from more upstreams.
      In this case, it's unnecessary for the strategy to record the Nack in its own StrategyInfo, because the Nack header is available on the PIT out-record in \textit{Nacked} field.
\end{itemize}


\subsubsection{Actions}\label{sec:strategy-action}

Actions are forwarding decisions made by the strategy.
An action is implemented as a non-virtual protected method of \texttt{nfd::fw::Strategy} class.

\paragraph{Send Interest Action}\label{sec:strategy-send-interest}\

This action enters the \textit{Outgoing Interest pipeline} (Section~\ref{sec:fw-outgoing-interest}).
It is implemented as \texttt{Strategy::sendInterest} method.
Parameters include: the affected PIT entry, the desired egress face and destination endpoint, and the Interest packet which must match the PIT entry.

The strategy is responsible for checking that forwarding of the Interest does not violate namespace-based scope control~\cite{ScopeControl}.
Typically, the strategy should invoke this action with one of the incoming Interest packets found in the PIT in-records, although the strategy may make a copy of an Interest and modify its guider fields, as long as the Interest still matches the PIT entry.

\paragraph{Send Data Action}\label{sec:strategy-send-data}\

This action deletes the PIT entry's in-record and enters the \textit{outgoing Data pipeline} (Section~\ref{sec:fw-outgoing-data}).
It is implemented as \texttt{Strategy::sendData} method.
Parameters include: the affected PIT entry, a Data packet, and a downstream face-endpoint pair which will be used as egress for the packet.

In many cases the strategy may want to send Data to every downstream.
The \texttt{Strategy::sendDataToAll} method is a helper for this purpose, which accepts a PIT entry, a Data packet, and the ingress FaceEndpoint containing the face where the packet came from and the endpoint of the sender.
Note that \texttt{sendDataToAll} will send the Data to each pending downstream, unless the pending downstream face is the same as the incoming face of the Data and the face is not \textit{ad hoc}.

\paragraph{Send Nack Action}\label{sec:strategy-send-nack}\

This action enters the \textit{outgoing Nack pipeline} (Section~\ref{sec:fw-outgoing-nack}).
It is implemented as \texttt{Strategy::sendNack} method.
Parameters include: the affected PIT entry, a downstream face-endpoint pair, and a Nack header.
An in-record for the downstream face should exist in the PIT entry, and a Nack packet will be constructed by taking the last incoming Interest from the PIT in-record and adding the specified Nack header.
If the PIT in-record is missing, this action has no effect.

In many cases the strategy may want to send a Nack to every downstream that still has an in-record.
The \texttt{Strategy::sendNacks} method is a helper for this purpose, which accepts a PIT entry and a Nack header.
Calling this helper method is equivalent to invoking \textit{send Nack} action for every downstream.


\subsubsection{Storage}\label{sec:strategy-storage}

Strategies are allowed to store arbitrary information on PIT entries, PIT in-records, PIT out-records, and Measurements entries, all of which are derived from \texttt{StrategyInfoHost} type\footnote{``Host'' is in the sense of holding strategy information, not an endpoint/network entity.} .
Inside the triggers, the strategy already has access to PIT entry and can lookup desired in-records and out-records.
Measurement entries (Section~\ref{sec:tab-m}) can be accessed via \texttt{Strategy::getMeasurements} method; a strategy's access is restricted to Measurements entries under the namespace(s) under its control (Section~\ref{sec:tab-m-usage}).

Strategy-specific information should be contained in a subclass of \texttt{StrategyInfo}.
At anytime, the strategy may call \texttt{getStrategyInfo}, \texttt{insertStrategyInfo}, and \texttt{eraseStrategyInfo} on a \texttt{StrategyInfoHost} to store and retrieve the information.
Note that the strategy must ensure that each \texttt{StrategyInfo} has a distinct TypeId; if the same TypeId is assigned to multiple types, NFD will most likely crash.

Since the strategy choice for a namespace can be changed at runtime, NFD ensures that all strategy-stored items under the transitioning namespace will be destroyed.
Therefore, the strategy must be prepared that some entities may not have strategy-stored items; however, if an item exists, its type is guaranteed to be correct.
The destructor of stored item must also cancel all timers, so that the strategy will not be activated on an entity that is no longer under its control.

Strategy is only allowed to store information using the above mechanism.
The strategy object (subclass of \texttt{nfd::fw::Strategy}) should otherwise be stateless.


\subsection{List of Strategies}\label{sec:strategy-builtin}

NFD comes with the following built-in strategies:

\begin{itemize}
\item Best route strategy (\texttt{/localhost/nfd/strategy/best-route}, Section~\ref{sec:strategy-best-route}) sends Interest to lowest cost upstream.
\item Multicast strategy (\texttt{/localhost/nfd/strategy/multicast}, Section~\ref{sec:strategy-multicast}) sends every Interest to every upstream.
\item Access router strategy (\texttt{/localhost/nfd/strategy/access}, Section~\ref{sec:strategy-access}) is designed for local site prefix on an access/edge router.
\item Adaptive SRTT-based Forwarding (ASF) strategy (\texttt{/localhost/nfd/strategy/asf}, Section~\ref{sec:strategy-asf}) sends Interests to the upstream with the lowest measured SRTT and periodically probes alternative upstreams.
\end{itemize}

\noindent
Since the objective of NFD is to provide a framework for easy experimentation, the list of the provided strategies is in no way comprehensive and we encourage implementation and experimentation of new strategies.
Section~\ref{sec:strategy-dev} provides insights to decide when implementation of a new strategy may be appropriate and give step-by-step guidelines explaining the process of developing new NFD strategies.


\input{strategy-bestroute}


\subsubsection{Multicast Strategy}\label{sec:strategy-multicast}

The multicast strategy forwards every Interest to all upstreams, indicated by the supplied FIB entry.
This strategy is implemented as \texttt{nfd::fw::MulticastStrategy} class.

\paragraph{Interest forwarding}
The strategy forwards (multicasts) a new Interest to all upstream faces from the list of nexthop records in the FIB entry
that do not violate scope.
Similar to Best Route Strategy (\ref{sec:strategy-best-route}), exponential retransmission suppression is used to prevent too many Interests.
A same Interest (same Name, Selectors, and Link) with different Nonce is suppressed if it's received during a retransmission suppression interval. A same Interest received after the suppression interval is treated as a new Interest.
However, unlike Best Route Strategy the suppression is done per upstream rather than per pit entry. For example: if NFD gets an interest on a face and the application wants to send the same interest back (with a new Nonce) within the suppression period, then it will not be suppressed for that face because upstream of the interest is different and no suppression has been calculated on it yet.

\paragraph{Nack generation}
If there is no eligible upstream, the Interest is rejected and a Nack with reason \textit{no route} is returned to the downstream.

\paragraph{Nack processing}
Nack processing is exactly the same as Best Route Strategy (\ref{sec:strategy-best-route}).


\input{strategy-access}


\input{strategy-asf}


\subsection{How to Develop a New Strategy}\label{sec:strategy-dev}

Before starting development of a new forwarding strategy, it is necessary to assess necessity of the new strategy, as well strategy capabilities and limitations (Section~\ref{sec:strategy-dev-necessity}).
The procedure of developing a new built-in strategy is outlined in Section~\ref{sec:strategy-dev-builtin}.


\subsubsection{Should I Develop a New Strategy?}\label{sec:strategy-dev-necessity}

In many network environments, it may be sufficient to use one of the existing strategies: best-route, multicast, access, or ASF.
In cases when an application wants a fine-grain control of Interest forwarding, it can use NDNLPv2's \texttt{NextHopFaceId} field to specify an outgoing face for every Interest.
However, this could control the outgoing face of local forwarder only.
In other cases, a new strategy development could be warranted, provided that the desired behavior can fit within the strategy API framework.

When developing a new strategy, one needs to remember that the strategy choice is local to a forwarder and only one strategy can be effective for the namespace.
Choosing the new strategy on a local forwarder will not affect the forwarding decisions on other forwarders.
Therefore, developing a new strategy may require reconfiguration of all network nodes.

The only purpose of the strategy is to decides how to forward Interests and cannot override any processing steps in the forwarding pipelines.
If it is desired to support a new packet type (other than Interest and Data), a new field in Interest or Data packets, or override some actions in the pipelines (e.g., disable Content Store lookup), it can be only accomplished by modification of the forwarding pipelines.

Even with the mentioned limitations, the strategy can provide a powerful mechanism to control how Data is retrieved in the network.
For example, by using a precise control of how and where Interests are forwarded and re-transmitted, a strategy can adapt Data retrieval for a specific network environment.
Another example would be an application of limits on how much Interests can be forwarded to which Faces.
This way a strategy can implement various congestion control and DDoS protections schemes~\cite{Yi:2013:A-Case-for-Stateful, Afanasyev:2013:Interest-Flooding}.


\subsubsection{Develop a New Strategy}\label{sec:strategy-dev-builtin}

The initial step in creating a new strategy is to create a class, say \texttt{MyStrategy} that is derived from \texttt{nfd::fw::Strategy}.
This subclass must at least override the \textit{triggers} that are marked pure virtual and may override other available \textit{triggers} that are marked just virtual.
The class should be placed in \path{daemon/fw} directory of NFD codebase, and needs to be compiled into NFD binary; dynamic loading of external strategy binary may be available in the future.

If the strategy needs to store information, it is needed to decide whether the information is related to a namespace or an Interest.
Information related to a namespace but not specific to an Interest should be stored in Measurements entries; information related to an Interest should be stored in PIT entries, PIT downstream records, or PIT upstream records.
After this decision is made, a data structure derived from \texttt{StrategyInfo} class needs to be declared.
In existing strategy classes, such data structures are declared as nested classes as it provides natural grouping and scope protection of the strategy-specific entity, but your strategy is not required to follow the same model.
If timers (Section~\ref{sec:common-scheduler}) are needed, \texttt{EventId} fields needs to be added to such data structure(s).

After creating the data structures, you may implement the \textit{triggers} with the desired strategy logic.
When implementing strategy logic, refer to Section~\ref{sec:strategy-trigger} describing when each trigger is invoked and what is it expected to do.

Notes and common pitfalls during strategy development:

\begin{itemize}
\item When retrieving a stored item from an entity, you should always check whether the retrieved element is \texttt{nullptr} (Section~\ref{sec:strategy-storage}).
      Otherwise, even the strategy logic guarantees that item will always be present on an entity, because NFD allows dynamic per-namespace strategy change, the expected item could not be there.
\item Measurements entries are cleaned up automatically.
      If Measurements entries are used, you need to call \texttt{this->\allowbreak getMeasurements()\allowbreak ->extendLifetime} to avoid an entry from being cleaned up prematurely.
\item \textit{Before satisfy Interest trigger} (Section~\ref{sec:strategy-before-satisfy-interest}) may be invoked with either pending Interest or recently satisfied Interest.
\item The strategy is allowed to retry, but retries should not be attempted after the PIT entry expires.
\item If implementing a timer, do not retain shared pointers of \texttt{Face}, \texttt{pit::Entry}, and other items.
      Instead, retain a weak pointer.
      In the timer callback, \texttt{.lock()} the weak pointer into a shared pointer, check it's not \texttt{nullptr} prior to use.
\item Timers must be cancelled in the destructor of the stored item (Section~\ref{sec:strategy-storage}).
      This is necessary to ensure that the strategy will not be accidentally triggered on an entity that is no longer being managed by the strategy.
\item The strategy is responsible for implementing namespace-based scope control.
\item The strategy is responsible for performing congestion control.
\end{itemize}

\noindent
Finally, make your strategy available for selection:
\begin{enumerate}
\item Choose an NDN name to identify the strategy.
      It is recommended to use a name like \path{/localhost/nfd/strategy/my-strategy} and append a version number.
      A version number component is required.
      The version number should be incremented whenever there is a non-trivial change to the strategy behavior.
\item Optionally, the strategy may accept parameters (Section~\ref{sec:tab-sc}).
      Parameters are used for things like ``minimum suppression timer'', ``ceiling of exponential back-off'', and so on.
      They are passed to the strategy constructor, as part of the strategy instance name after the version component.
\item Register the strategy class with \texttt{NFD\_REGISTER\_STRATEGY} macro.
\end{enumerate}
After that, the strategy is ready to use and can be activated by a Strategy Choice management command (Section~\ref{sec:mgmt-sc}), such as \texttt{nfdc set-strategy}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
