\subsubsection{Best Route Strategy}\label{sec:strategy-best-route}

The best route strategy forwards an Interest to the eligible Face with the lowest routing cost.
This strategy is implemented as \texttt{nfd::fw::BestRouteStrategy} class.

% VERSION 1 BEHAVIOR

% The strategy forwards new Interests only; if an Interest is not new (unexpired out-record exists), it is not forwarded.
% For a new Interest, the list of nexthop records is consulted to find an \textit{eligible} (Section~\ref{sec:strategy-multicast}) upstream with lowest routing cost, and the Interest is forwarded to that face.
% If there is no eligible upstream, the Interest is rejected.

\paragraph{Interest forwarding}

The strategy forwards a new Interest to the lowest-cost eligible Face.
After the new Interest is forwarded, a similar Interest with same Name, Selectors, and Link but different Nonce would be suppressed if it's received during a retransmission suppression interval.
However, a similar Interest received after the suppression interval is called a ``retransmission'', and is forwarded to the lowest-cost eligible Face that is not previously used; if all Faces have been used, it is forwarded to an eligible Face that was used earliest.

Note that applying the suppression and retransmission mechanisms does not distinguish whether the Interest come from the same downstream or a different downstream.
Although the former is typically a retransmission from the same consumer and the latter is typically from a different consumer making use of NDN's built-in Data multicast, there's no prominent difference in terms of forwarding, so they are processed alike.

\paragraph{Retransmission suppression interval}

Instead of forwarding every incoming Interest, the retransmission suppression interval is imposed to prevent a malicious or misbehaving downstream from sending too many Interests end-to-end.
The retransmission suppression interval should be chosen so that it permits reasonable consumer retransmissions, while prevents DDoS attacks by overly frequent retransmissions.

We have identified three design options for setting the retransmission suppression interval:

\begin{itemize}[nosep]
\item A \textbf{fixed interval} is the simplest, but it is hard to find a balance between reasonable consumer retransmissions and DDoS prevention.
\item Doing \textbf{RTT estimation} would allow a retransmission after the strategy believes the previous forwarded Interest is lost or otherwise won't be answered, but RTT estimations are not reliable, and in case the consumer applications are also using RTT estimation to schedule their retransmissions, this results in double control loop and potentially unstable behavior.
\item Using \textbf{exponential back-off} gives consumer applications control over the retransmission and also effectively prevents DDoS. Starting with a short interval, the consumer can retransmit quickly in a low-RTT communication scenario. The interval goes up after each accepted retransmission, so an attacker cannot abuse the mechanism by retransmitting too frequently.
\end{itemize}

We finally settled with the exponential back-off algorithm.
The initial interval is set to 10~milliseconds.
After each retransmission being forwarded, the interval is doubled until it reaches a maximum of 250~milliseconds.

\paragraph{Nack generation}

The best route strategy uses Nack to improve its performance. When an Interest cannot be forwarded to an eligible Face, a Nack will be returned to the incoming Face to notify the downstream.

The eligibility of a Face depends on its type.
An ad hoc Face is \textit{eligible} if forwarding an Interest to it does not violate scope~\cite{ScopeControl}. Additionally, for a multicast or point-to-point Face, besides the scope check, the outgoing Face must not be the incoming Face.
If there are no eligible Faces available, the strategy rejects the Interest and returns a Nack to the downstream with reason \textit{NoRoute}.

Currently, the Nack packet does not indicate which prefix that the node has no route to reach, because it is not trivial to compute this prefix.
Also, Nacks will not be returned to multicast or ad hoc Faces.

\paragraph{Nack processing}

Upon receiving an incoming Nack, the strategy itself does not retry the Interest with other nexthops (because ``best route'' forwards to only one nexthop for each incoming Interest), but informs the downstream(s) as quickly as possible.
If the downstream/consumer wants, it can retransmit the Interest, and the strategy would retry it with another nexthop.

Specifically, depending on the situation of other upstreams, the strategy takes one of these actions:

\begin{itemize}[nosep]
\item If all pending upstreams have Nacked, a Nack is sent to all downstreams.
\item If all but one pending upstream have Nacked, and that upstream is also a downstream, a Nack is sent to that downstream.
\item Otherwise, the strategy continues waiting for the arrival of more Nacks or Data.
\end{itemize}

To determine what situation a PIT entry is in, the strategy makes use of the \textit{Nacked} field (Section~\ref{sec:tab-pit-entry}) on PIT out-records, and does not require extra measurement storage.

The second situation, ``all but one pending upstream have Nacked and that upstream is also a downstream'', is introduced to address a specific ``live deadlock'' scenario, where two hosts are waiting for each other to return the Nack.
More details about this scenario can be found at \url{https://redmine.named-data.net/issues/3033#note-7}.

In the first situation, the Nack returned to downstreams need to indicate a reason.
In the easy case where there's only one upstream, the reason from this upstream is passed along to downstream(s).
When there are multiple upstreams, the \emph{least severe} reason is passed to downstream(s), where the severity of Nack reasons are defined as: Congestion \textless{} Duplicate \textless{} NoRoute.
For example, one upstream has returned Nack-NoRoute and the other has returned Nack-Congestion.
This forwarder choose to tell downstream(s) ``congestion'' so that they can retry with this path after reducing their Interest sending rate, and this forwarder can forward the retransmitted Interests to the second upstream at a slower rate and hope it's no longer congested.
If we instead tell downstream(s) ``no route'', it would make downstreams believe that this forwarder cannot reach the content source at all, which is inaccurate.
