\subsection{Strategy Choice Table}\label{sec:tab-sc}

The Strategy Choice Table contains the forwarding strategy (Section~\ref{sec:strategy}) chosen for each namespace.
This table is a new addition to the NDN architecture.
Theoretically, forwarding strategy is a program that is supposed to be stored in FIB entries~\cite{Jacobson:2009:NNC:1658939.1658941}.
In practice, we find that it is more convenient to save the forwarding strategy in a separate table, instead of storing it with FIB entry, for the following reasons:
\begin{itemize}
\item FIB entries come from RIB entries, which are managed by the NFD-RIB service (Section~\ref{sec:rib}).
      Storing the strategy in FIB entries would require the RIB service to create/update/remove strategies when it manipulates the FIB. Therefore, this would increase the complexity of the RIB service.
\item FIB entry is automatically deleted when the last NextHop record is removed, including when the last upstream face fails.
      However, we don't want to lose the configured strategy.
\item The granularity of strategy configuration is different from the granularity of RIB entry or FIB entry.
      Having both in the same table makes inheritance handling more complex.
\end{itemize}

The structure, semantics, and algorithms of Strategy Choice Table is outlined in Section~\ref{sec:tab-sc-struct}.
How Strategy Choice Table is used by rest of NFD is described in Section~\ref{sec:tab-sc-usage}.
The implementation of Strategy Choice Table algorithms is discussed in Section~\ref{sec:tab-nt}.


\subsubsection{Structure and Semantics}\label{sec:tab-sc-struct}

\paragraph{Strategy Choice entry}\

A Strategy Choice entry (\texttt{nfd::strategy\_choice::Entry}) contains a name prefix and a forwarding strategy chosen for that namespace.
A \textit{nfd::fw::Strategy} subclass instance is created at runtime, and stored inside each Strategy Choice entry.

\paragraph{Strategy Choice Table}\

The Strategy Choice Table (\texttt{nfd::StrategyChoice}) is a collection of Strategy Choice entries.
There can be only one strategy set per namespace, but sub-namespaces can have their own choices for the strategy.

In order to guarantee that every namespace has a strategy, NFD always insert the root entry for \texttt{/} namespace to the Strategy Choice Table during initialization.
The strategy chosen for this entry, called the \textit{default strategy}, is defined by the hard-coded \texttt{getDefaultStrategyName} free function in \path{daemon/fw/forwarder.cpp}.
The default strategy can be replaced, but the root entry in Strategy Choice Table can never be deleted.

The insertion operation (\texttt{StrategyChoice::insert}) inserts a Strategy Choice entry, or updates the chosen strategy on an existing entry.
This operation accepts a name prefix on which the strategy choice is applied, and a strategy instance name.
The strategy instance name starts with a name prefix that indicates the strategy ``program'', such as \path{/localhost/nfd/strategy/best-route}; then, it may contain an optional version number to choose a specific version of the strategy program, and additional name components as ``parameters'' which are passed to the strategy constructor.
The strategy instance name is used to locate a strategy type in the strategy registry (within \texttt{nfd::fw::Strategy} class); if a strategy type is found, it is instantiated and stored into the Strategy Choice entry.

The deletion operation (\texttt{StrategyChoice::erase}) deletes a Strategy Choice entry.
The namespace covered by the deletes would inherit the strategy defined on the parent namespace.
It is disallowed to delete the root entry.

The usual exact match operation is supported.
Strategy Choice entries can be iterated over in a forward iterator, in unspecified order.

\textbf{Find Effective Strategy} algorithm (\texttt{StrategyChoice::findEffectiveStrategy}) finds a strategy that should be used to forward an Interest.
The effective strategy for the namespace can be defined as follows:
\begin{itemize}[nosep]
\item If the namespace is explicitly associated with the strategy, then this is the effective strategy.
\item Otherwise, the first parent namespace for which strategy was explicitly set defines the effective strategy.
\end{itemize}

The find effective strategy algorithm takes a Name, a PIT entry, or a measurements entry as input parameter.
The return value of the algorithm is a forwarding strategy that is found by longest prefix match using the supplied name.
This return value is always a valid entry, because every namespace must have a strategy.


\subsubsection{Usage}\label{sec:tab-sc-usage}

The Strategy Choice Table is updated only through management protocol.
Strategy Choice manager (Section~\ref{sec:mgmt-sc}) is directly responsible for updating the Strategy Choice Table.

The Strategy Choice is expected to be stable, as strategies are expected to be manually chosen by the local NFD operator (either user for personal computers or system administrators for the network routers).

The effective strategy search algorithm is used by forwarding in the \textit{Content Store miss pipeline} (Section~\ref{sec:fw-cs-miss}), the \textit{incoming Data pipeline} (Section~\ref{sec:fw-incoming-data}), and the \textit{incoming Nack pipeline} (Section~\ref{sec:fw-incoming-nack}).
It is called at most twice per incoming packet.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
